$(function() {
    // 搜尋列表
    $("#searchForm").submit(function(){
        var url = $(this).attr('action');
        $("#searchForm > input").each(function(){
            url += '&'+$(this).attr('name')+'='+encodeURIComponent($(this).val());
        });
        location = url;
        return false;
    });
    
    // 推薦賽事
    var $container = $('.albumWrap');
    $container.imagesLoaded(function(){
        $(".albums").fadeIn();
        $container.masonry({
            itemSelector: '.albums'
        });
    });
  
    //heart Toggle
    $('.btnHeart').click(function(){
        // 紀錄點擊
        if($(this).hasClass('unHeart')){
            var pid = $('[name="targetPid"]').val();
            var pidArr = new Array();
            if($.cookie("productLike")){
                pidArr = $.cookie("productLike").split(',');
            }
            console.log(pidArr);
            if($.inArray(pid,pidArr)){
                console.log(123);
                $.ajax({
                    url: 'index.php?route=photo/photo/ajaxClick&act=ajax&product='+pid,
                    success: function (data){
                        pidArr[pidArr.length] = pid;
                        $.cookie("productLike", pidArr.join(','), {expires:1});
                    },
                    error:function(resp){
                        console.log(resp);
                    }
                });
            }
        }
        $(this).toggleClass('unHeart');
    });

	// 拍攝時間搜尋
    var min = parseInt($('[name="minTime"]').val().split(':')[0])*60+parseInt($('[name="minTime"]').val().split(':')[1]);
    var max = parseInt($('[name="maxTime"]').val().split(':')[0])*60+parseInt($('[name="maxTime"]').val().split(':')[1]);
    var start = parseInt($('[name="stime"]').val().split(':')[0])*60+parseInt($('[name="stime"]').val().split(':')[1]);
    var end = parseInt($('[name="etime"]').val().split(':')[0])*60+parseInt($('[name="etime"]').val().split(':')[1]);
    $( "#timeSlider" ).slider({
        range: true,
        min: min,
        max: max,
        step: 5,
        values: [ start, end ],
        slide: function( event, ui ) {
            var startHours = checkTime(Math.floor(ui.values[ 0 ] / 60));
            var startMinutes  = checkTime(ui.values[ 0 ] - (startHours * 60));
            var endHours = checkTime(Math.floor(ui.values[ 1 ] / 60));
            var endMinutes = checkTime(ui.values[ 1 ] - (endHours * 60));
            $('[name="stime"]').val(startHours + ":" + startMinutes);
            $('[name="etime"]').val(endHours + ":" + endMinutes);
            $( "#startTime" ).val(startHours + ":" + startMinutes);
            $( "#endTime" ).val(endHours + ":" + endMinutes);
            $("#searchForm").submit();
        }
    });
    function checkTime(time){
		var timeString = time.toString();
		if(timeString.length == 1){
			timeString = '0' + timeString;
		}
		return timeString;
	}

    // 照片詳情
    var wContainer = -1;
    var wColumn = -1;
    var columnsInRow = -1;
    var currentColumn = -1;
    var currentRow = -1;
    var prevItem = -1;
    var lastItem = -1;
    var insertItem = -1;
    var row = -1;
    var column = -1;
    var $containerphoto = $('.photoWrap');
    var photosTrigger = $('.photosTrigger');
    var detailsBox = $('.detailsBox');
    var length = photosTrigger.length;
    
    photosTrigger.click(function() {
        if(length > 0){
            checkPos(this);
            if(column == -1){
                //first time
                detailsBox.insertAfter(photosTrigger.eq(insertItem)).stop().slideToggle(300);
                $(this).addClass('current isExpanded');
                photoDetail(this);
            }else{
                if (column == currentColumn) {
                    //same column
                    detailsBox.stop().slideToggle(300);
                    $(this).toggleClass('isExpanded');
                }else{
                    var isExpanded = $('.photos.current').hasClass('isExpanded');
                    if(row != currentRow){
                        //diffenent column & row
                        detailsBox.stop().slideUp(100,function(){
                            detailsBox.insertAfter(photosTrigger.eq(insertItem)).stop().slideDown(300);
                        });
                    }else if(row == currentRow && !isExpanded){
                        //diffenent column same row 
                        detailsBox.stop().slideDown(300);
                    }
                    $('.photos.current').removeClass('current isExpanded');
                    $(this).addClass('current isExpanded');
                    photoDetail(this);
                }
            }
            row = currentRow;
            column = currentColumn;
        }
    });
    
    $('.next').click(function(){
        var next = photosTrigger.eq(photosTrigger.index($('.photos.current'))+1);
        checkPos(next);
        changeSlider(next);
        photoDetail(next);
    });
    
    $('.prev').click(function(){
        var prev = photosTrigger.eq(photosTrigger.index($('.photos.current'))-1);
        checkPos(prev);
        changeSlider(prev);
        photoDetail(prev);
    });
    
    $('.btn_close').click(function(){
        detailsBox.stop().slideToggle(300);
        $('.photos.current').toggleClass('isExpanded');
    });
    
    $(window).resize(function(){
        var current = $('.photos.current');
        if (length > 0 && current.length > 0){
            var isExpanded = current.hasClass('isExpanded');
        
            if(isExpanded){
                checkPos(current);
                detailsBox.insertAfter(photosTrigger.eq(insertItem));
            }
        }
    });
    
    function checkPos(current){
        wContainer = $containerphoto.width();
        wColumn = photosTrigger.eq(0).outerWidth(true);
        columnsInRow = parseInt(wContainer/wColumn);
        currentColumn = photosTrigger.index(current);
        currentRow = parseInt(currentColumn/columnsInRow);
        prevItem = (currentRow+1)*columnsInRow-1;
        lastItem = photosTrigger.length-1;
    
        if(prevItem <= lastItem){
            insertItem = prevItem;
        }else{
            insertItem = lastItem;
        }
        
        $(".btn_left").show();
        $(".btn_right").show();
        if(currentColumn==lastItem){
            $(".btn_right").hide();
        }
        if(currentColumn==0){
            $(".btn_left").hide();
        }
    }
    
    function changeSlider(changeItem){
        var isExpanded = $('.photos.current').hasClass('isExpanded');
        if(row != currentRow){
            //diffenent column & row
            detailsBox.stop().slideUp(100,function(){
                detailsBox.insertAfter(photosTrigger.eq(insertItem)).stop().slideDown(300);
            });
        }else if(row == currentRow && !isExpanded){
            //diffenent column same row 
            detailsBox.stop().slideDown(300);
        }
        $('.photos.current').removeClass('current isExpanded');
        changeItem.addClass('current isExpanded');
        
        row = currentRow;
        column = currentColumn;
    }
    
    // 更換照片明細
    function photoDetail(obj){
        var target = photosTrigger.index($('.photos.current'));
        for(var i=0;i<imageData.length;i++){
            if(target==i){
                $('[name="targetPid"]').val(imageData[i].photo_id);
                detailsBox.find('.details_img > img').attr('src',imageData[i].photo_cover);
                detailsBox.find('.details_index > a').attr('href','http://www.facebook.com/share.php?u='+encodeURIComponent(imageData[i].photo_share.replace(/\&amp;/g, '&')));
                detailsBox.find('a.addToCart').attr('href','index.php?route=photo/photo/download&'+imageData[i].photo_param);
                detailsBox.find('a.favorite').attr('href','index.php?route=photo/photo/collection&'+imageData[i].photo_param);
                detailsBox.find('.details_number').html('照片編號：'+imageData[i].photo_id);
                detailsBox.find('.details_bib').html('號碼布：'+imageData[i].photo_bib);
                detailsBox.find('.details_time').html('拍攝時間：'+imageData[i].photo_date);
                // 照片序號
                var shareItem = detailsBox.find('.details_index > a')[0].outerHTML;
                var totalItem = detailsBox.find('.details_index > span')[0].outerHTML;
                detailsBox.find('.details_index').html(shareItem+imageData[i].photo_serial+totalItem);
                // LIKE Button SHOW
                detailsBox.find('.btnHeart').addClass('unHeart');
                var pidArr = new Array();
                if($.cookie("productLike")){
                    pidArr = $.cookie("productLike").split(',');
                }
                if(pidArr.length>0 && !$.inArray(imageData[i].photo_id,pidArr)){
                    detailsBox.find('.btnHeart').removeClass('unHeart');
                }
            }
        }
    }

});