<?php echo $header; ?>
    <div class="wrapper">
		<div class="header">
            <?php echo $header_bar; ?>
        </div>
		<div class="content searchPhoto">
			<div class="contentTop">
                <ul class="breadCrumb">
					<li><a href="https://www.eventpal.com.tw">首頁</a></li>
					<li><a href="index.php">相片咖</a></li>
                    <li><?php echo $album["name"];?></li>
				</ul>
                <div class="searchWrap">
                    <div class="searchBox">
                        <form action="<?php echo $search_action;?>" method="get" id="searchForm" class="searchForm clearfix" >
                            <input type="hidden" name="album" value="<?php echo $album["id"];?>">
                            <input type="hidden" name="stime" value="<?php echo $stime;?>">
                            <input type="hidden" name="etime" value="<?php echo $etime;?>">
                            <input type="search" name="bib" id="search" placeholder="輸入號碼布" value="<?php echo $bib;?>">
                            <a class="searchKeyword" href="#" onclick="$('#searchForm').submit();">
                                <div class="btnSearch"></div>
                            </a>
                        </form>
                    </div>
                </div>
                <div class="albumInfo clearfix">
					<h1 class="albumTitle"><?php echo $album["name"];?></h1>
					<div class="filterWrap">
						<span class="filterTitle">拍攝時間</span>
						<input type="text" id="startTime" readonly value="<?php echo $stime;?>"/>
						<div id="timeSlider" style="display: inline-block;"></div>
						<input type="text" id="endTime" readonly value="<?php echo $etime;?>"/>
					</div>
					<div class="socialBox">
						<a href="http://www.facebook.com/share.php?u=<?php echo urlencode($album["link"]."&album=".$album["id"]);?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" class="share_btn share_fb left">
	                        <i class="fab fa-facebook-f fa-lg"></i>
                    	</a>
                    	<a href="https://social-plugins.line.me/lineit/share?url=<?php echo urlencode($album["link"]."&album=".$album["id"]);?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" class="share_btn share_line left">
							<i class="fab fa-line fa-3x"></i>
						</a>
					</div>
                    <span class="albumDate">拍攝日期:<?php echo $album["date"];?></span>
				</div>
            </div>
            <?php if(!count($photos)):?>
            <div class="resultWrap">
				<div class="result noResult">
					<div class="iconNoResult"></div>
					<h3 class="resultMsg">"<span class="keyword"><?php echo $bib;?></span>" 查無相關結果<br />更多熱門賽事推薦給您</h3>
				</div>
			</div>
            <?php if(count($activitys)):?>
                <div class="albumWrap">
                <?php foreach($activitys as $k => $v):?>
                <a class="albums ad" href="<?php echo $v["link"];?>">
					<div class="album_img" style="background-image:url(<?php echo $v["img"];?>);"></div>
					<div class="album_info">
						<h2 class="album_title"></h2>
					</div>
				</a>
                <?php endforeach;?>
                </div>
            <?php endif;?>
            <?php else:?>
			<div class="photoWrap clearfix">
                <!-- 照片 -->
                <?php foreach($photos as $k => $v):?>
                <div class="photos photosTrigger" style="background-image: url(<?php echo $v["photo_img"];?>);">
					<div class="photo_info">
						<p class="photo_number">NO：<?php echo $v["photo_id"];?></p>
						<p class="photo_bib">BIB：<?php echo $v["photo_bib"];?></p>
						<p class="photo_time">TIME：<?php echo $v["photo_time"];?></p>
					</div>
					<span class="boxArrow"></span>
				</div>
                <?php endforeach;?>
                <!-- 明細 -->
                <div class="detailsBox">
					<div class="photo_details">
						<div class="details_img clearfix">
							<img src="image/catalog/album_default.jpg" alt="">
							<div class="heartWrap">
								<span class="btnHeart unHeart"></span>
							</div>
						</div>
						<div class="details">
							<div class="details_index">
								<a href="http://www.facebook.com/share.php?u=[[[PHOTOLINK]]]" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" class="share_btn share_fb circle">
		                        	<i class="fab fa-facebook-f fa-sm"></i>
	                    		</a>
	                    		20<span class="">/<?php echo $totalCnt;?></span>
	                    	</div>
	                    	<div class="btnWrap">
	                    		<span class="btn_left prev"></span>
								<span class="btn_right next"></span>
	                    	</div>
							<div class="details_info">
								<p class="details_number">照片編號：123456789</p>
								<p class="details_bib">號碼布：2415</p>
								<p class="details_time">拍攝時間：2017/05/17 07:12</p>
								<!--p class="details_price">售價：NT$50/35點</p-->
								<!--p class="details_qty">數量：</p-->
							</div>
							<div>
								<a class="addToCart" href="#">免費下載</a>
								<a class="favorite" href="#">加入收藏</a>
							</div>
							<!--div class="details_ad">
								<img src="images/album/ad-2.jpg" alt="ad">
							</div-->
						</div>
						<span class="btn_close"></span>
					</div>
				</div>
                <!-- 分頁 -->
                <?php echo $pagination;?>
            </div>
            <?php endif;?>
            <input type="hidden" name="minTime" value="<?php echo $album["minTime"];?>">
            <input type="hidden" name="maxTime" value="<?php echo $album["maxTime"];?>">
            <input type="hidden" name="targetPid" value="">            
		</div>
        <?php echo $footer; ?>
	</div>
    <link rel="stylesheet" type="text/css" href="catalog/view/stylesheet/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="catalog/view/stylesheet/jquery-ui.min.css">
    <script type="text/javascript" src="catalog/view/javascript/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/jquery/jquery.cookie.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/album.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/jquery/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript">
        var imageData = <?php echo json_encode($photos);?>;
        $(document).ready(function(){
            <?php if($photo):?>
            $('.photosTrigger').trigger('click');
            <?php endif;?>
        });
    </script>
</body>
</html>