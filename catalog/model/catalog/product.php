<?php
class ModelCatalogProduct extends Model{
    
    /**
	 * [getTotalCnt 取相片數量]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2018-03-01
	 */
	public function getTotalCnt( $data = array()) {
        $filterArr = array();
        
        if(isset($data["album_id"]) && !empty($data["album_id"])){
            $filterArr[] = "p.album_id=".intval($data["album_id"]);
        }
        if(isset($data["product"]) && !empty($data["product"])){
            $filterArr[] = "p.product_id=".intval($data["product"]);
        }
        if(isset($data["stime"]) && !empty($data["stime"])){
            $filterArr[] = "p.date_photo >='".$this->db->escape($data["stime"])."'";
        }
        if(isset($data["etime"]) && !empty($data["etime"])){
            $filterArr[] = "p.date_photo <='".$this->db->escape($data["etime"])."'";
        }
        if(isset($data["bib"]) && !empty($data["bib"])){
            $filterArr[] = "b.bib_number='".$this->db->escape($data["bib"])."'";
        }
        if(isset($data["status"])){
            $filterArr[] = "p.status=".intval($data["status"]);
        }
        
        $SQLCmd = "SELECT COUNT(distinct p.product_id) AS total ".
            "FROM ". DB_PREFIX ."product p ".
            "LEFT JOIN ". DB_PREFIX ."product_to_bib b using(product_id) WHERE 1=1";
        if(count($filterArr)){
            $SQLCmd = "SELECT COUNT(distinct p.product_id) AS total ".
            "FROM ". DB_PREFIX ."product p ".
            "LEFT JOIN ". DB_PREFIX ."product_to_bib b using(product_id) ".
            "WHERE ".implode(" AND ",$filterArr);
        }
		//$SQLCmd .= " GROUP BY p.product_id";
        
        //echo $SQLCmd;exit;
		$query = $this->db->query( $SQLCmd) ;
		return isset($query->row['total']) ?  $query->row['total'] : 0;
	}
    
    /**
	 * [getLists 取相片列表]
	 * @param   array   $data [description]
	 * @return  [type]        [description]
	 * @Another Nicole
	 * @date    2018-03-29
	 */
	public function getLists($data = array()) {
        $filterArr = array();
        
        if(isset($data["album_id"]) && !empty($data["album_id"])){
            $filterArr[] = "p.album_id=".intval($data["album_id"]);
        }
        if(isset($data["product"]) && !empty($data["product"])){
            $filterArr[] = "p.product_id=".intval($data["product"]);
        }
        if(isset($data["stime"]) && !empty($data["stime"])){
            $filterArr[] = "p.date_photo >='".$this->db->escape($data["stime"])."'";
        }
        if(isset($data["etime"]) && !empty($data["etime"])){
            $filterArr[] = "p.date_photo <='".$this->db->escape($data["etime"])."'";
        }
        if(isset($data["bib"]) && !empty($data["bib"])){
            $filterArr[] = "b.bib_number='".$this->db->escape($data["bib"])."'";
        }
        if(isset($data["status"])){
            $filterArr[] = "p.status=".intval($data["status"]);
        }
        
        $SQLCmd = "SELECT p.*,GROUP_CONCAT(b.bib_number separator ',') as bib ".
            "FROM ". DB_PREFIX ."product p ".
            "LEFT JOIN ". DB_PREFIX ."product_to_bib b using(product_id) WHERE 1=1";
        if(count($filterArr)){
            $SQLCmd = "SELECT p.*,GROUP_CONCAT(b.bib_number separator ',') as bib ".
            "FROM ". DB_PREFIX ."product p ".
            "LEFT JOIN ". DB_PREFIX ."product_to_bib b using(product_id) ".
            "WHERE ".implode(" AND ",$filterArr);
        }
        
		$SQLCmd .= " GROUP BY p.product_id ORDER BY p.date_photo";
        
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$SQLCmd .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->rows ;
	}
    
    /**
	 * [getProduct 取相片內容]
	 * @param   string  $product_id   [description]
	 * @return  [type]              [description]
	 * @Another Nicole
	 * @date    2018-03-30
	 */
	public function getProduct( $product_id) {
		$SQLCmd = "SELECT p.*,GROUP_CONCAT(b.bib_number separator ',') as bib ".
            "FROM ". DB_PREFIX ."product p ".
            "LEFT JOIN ". DB_PREFIX ."product_to_bib b using(product_id) WHERE p.product_id=".intval($product_id)." AND p.status=1";
		$query = $this->db->query( $SQLCmd) ;
		return $query->row;
	}
    
    /**
	 * [getProductTimes 取相簿開始與結束時間]
	 * @param   string  $album_id   [description]
	 * @return  [type]              [description]
	 * @Another Nicole
	 * @date    2018-03-30
	 */
    public function getProductTimes( $album_id){
        $SQLCmd = "SELECT MAX(DATE_FORMAT(date_photo,'%H:%i')) as maxTime,".
            "MIN(DATE_FORMAT(date_photo,'%H:%i')) as minTime ".
            "FROM ". DB_PREFIX ."product WHERE album_id=".intval($album_id)." AND status=1" ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->row;
    }
    
    /**
	 * [insertClickRecord 新增相片點擊紀錄]
	 * @param   array  $data    [description]
	 * @return  [type]          [description]
	 * @Another Nicole
	 * @date    2018-04-25
	 */
    public function insertClickRecord( $data){
        $insertArr = array();
        if(isset($data["item_id"])){
            $insertArr[] = "item_id=".intval($data["item_id"]);
        }
        if(isset($data["type"])){
            $insertArr[] = "i_type=".intval($data["type"]);
        }
        if(!count($insertArr)){
            return false;
        }
        $insertArr[] = "date_added='".date("Y-m-d H:i:s")."'";
        
        $this->db->query("INSERT INTO ". DB_PREFIX ."click_record SET ".implode(",",$insertArr));
        
        return $this->db->getLastId();
    }
}
