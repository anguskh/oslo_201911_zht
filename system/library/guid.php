<?php
/**
 *
 * @Another Angus
 * @date    2017-10-23
 */
class Guid {

	public function __construct() {
	}
	/**
	 * [getGUID guid產生器]
	 * @param   boolean    $mod [true:產出; false:不產出]
	 * @return  [type]          [回傳 guid : A190129D-00F7-5CAC-FDB6-76969B3EEEC8]
	 * @Another Angus
	 * @date    2017-10-23
	 */
	public function getGUID( $mod = true) {
		if (function_exists('com_create_guid')){
			return com_create_guid();
		} else {
			mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
			$charid = strtoupper(md5(uniqid(rand(), true)));
			$hyphen = chr(45);// "-"
			if ( $mod) {
				$uuid = chr(123)// "{"
					.substr($charid, 0, 8).$hyphen
					.substr($charid, 8, 4).$hyphen
					.substr($charid,12, 4).$hyphen
					.substr($charid,16, 4).$hyphen
					.substr($charid,20,12)
					.chr(125);// "}"
			} else {
				$uuid = substr($charid, 0, 8).$hyphen
					.substr($charid, 8, 4).$hyphen
					.substr($charid,12, 4).$hyphen
					.substr($charid,16, 4).$hyphen
					.substr($charid,20,12) ;
			}
		return $uuid;
		}
	}
}