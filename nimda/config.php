<?php
// HTTP
define('SERVER_ADDRESS',	$_SERVER['HTTP_HOST']) ;
define('PROJECT_SITE',		'zht') ;

define('HTTP_SERVER',		'http://'.SERVER_ADDRESS.'/'.PROJECT_SITE.'/nimda/') ;
define('HTTP_CATALOG',		'http://'.SERVER_ADDRESS.'/'.PROJECT_SITE.'/') ;

// HTTPS
define('HTTPS_SERVER',		'http://'.SERVER_ADDRESS.'/'.PROJECT_SITE.'/nimda/') ;
define('HTTPS_CATALOG',		'http://'.SERVER_ADDRESS.'/'.PROJECT_SITE.'/') ;

// DIR
define('DIR_INITIAL',		'/var/www/html/'.PROJECT_SITE) ;

define('DIR_APPLICATION',	DIR_INITIAL.'/nimda/');
define('DIR_SYSTEM',		DIR_INITIAL.'/system/');
define('DIR_IMAGE',			DIR_INITIAL.'/image/');
define('DIR_LANGUAGE',		DIR_INITIAL.'/nimda/language/');
define('DIR_TEMPLATE',		DIR_INITIAL.'/nimda/view/template/');
define('DIR_CONFIG',		DIR_INITIAL.'/system/config/');
define('DIR_CACHE',			DIR_INITIAL.'/system/storage/cache/');
define('DIR_DOWNLOAD',		DIR_INITIAL.'/system/storage/download/');
define('DIR_LOGS',			DIR_INITIAL.'/system/storage/logs/');
define('DIR_MODIFICATION',	DIR_INITIAL.'/system/storage/modification/');
define('DIR_UPLOAD',		DIR_INITIAL.'/system/storage/upload/');
define('DIR_CATALOG',		DIR_INITIAL.'/catalog/');

// DB
define('DB_DRIVER',		'mysqli');
define('DB_HOSTNAME',	'anguskh.com');
define('DB_USERNAME',	'oslo');
define('DB_PASSWORD',	'oslo201910');
define('DB_DATABASE',	'oslo_zht');
define('DB_PORT',		'3306');
define('DB_PREFIX',		'oc_');

// Debug Mod add by Angus 2017.03.05
define('DB_DEBUG_MOD', true) ;