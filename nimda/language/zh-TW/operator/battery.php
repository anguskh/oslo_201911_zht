<?php
// Heading
$_['heading_title']     = '电池管理';

// Text
$_['text_success']      = '更新成功';
$_['text_list']         = '列表';
$_['text_add']          = '新增';
$_['text_edit']         = '編輯';

// Column
$_['column_battery_id']               = '电池序号';
$_['column_DorO_flag']                = '厂商类型';
$_['column_do_num']                   = '经销商/营运商';
$_['column_sv_num']                   = '车辆';
$_['column_manufacture_date']         = '出厂日期';
$_['column_position']                 = '电池位置';
$_['column_map']                      = '地图';
$_['column_exchange_count']           = '电池交换<br>累积次数';
$_['column_last_report_datetime']     = '最后一次回报时间';
$_['column_station']                  = '電池交換站地点名称<br>（电池交換站序号）'; // ??
$_['column_abc']                      = '轨道'; // ??
$_['column_history']                  = '租借历程'; // ??
$_['column_SOC']                      = 'SOC'; // ??
$_['column_SOH']                      = 'SOH'; // ??
$_['column_status']                   = '电池状态';
$_['column_battery_station_position'] = '基站位置<br>(MCC，MNC，LAC，CELLID)';
$_['column_battery_gps_manufacturer'] = 'GPS 制造商';
$_['column_battery_gps_version']      = 'GPS 固件版本';

$_['column_action']      = '動作';

// Entry
$_['entry_username']   	= '帳號';
$_['entry_user_group'] 	= '類別';
$_['entry_password']   	= '密碼';
$_['entry_confirm']    	= '確認密碼';
$_['entry_firstname']  	= '名字';
$_['entry_lastname']   	= '姓氏';
$_['entry_email']      	= '電子郵件';
$_['entry_image']      	= '照片';
$_['entry_status']     	= '狀態';

// Error
$_['error_permission'] 	= '您沒有權限更改使用者的設置!';
$_['error_account']    	= '不能被刪除，這是目前的登入帳號!';
$_['error_exists']     	= '您輸入的帳號已註冊過，請重新輸入!';
$_['error_username']   	= '帳號必須在3到20個字元之間!';
$_['error_password']   	= '密碼必須在4到20個字元之間!';
$_['error_confirm']    	= '密碼和確認密碼不符!';
$_['error_firstname']  	= '名字必須在1到32個字元之間!';
$_['error_lastname']   	= '姓氏必須在1到32個字元之間!';
$_['error_email']           = '錯誤的電子郵件格式!';
$_['error_exists_email']    = '您輸入的電子郵件已註冊過，請重新輸入!';