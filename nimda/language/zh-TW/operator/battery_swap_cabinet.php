<?php
// Heading
$_['heading_title']     = '機櫃管理';

// Text
$_['text_success']      = '更新成功';
$_['text_list']         = '列表';
$_['text_add']          = '新增';
$_['text_edit']         = '編輯';
$_['text_delete']       = '刪除';

// Column
$_['column_bss_no']       = '機櫃編號';
$_['column_bss_id']       = '電池交換站序號';
$_['column_so_num']     = '营运商';
$_['column_ss_num']     = '電池交換站地点名称';
$_['column_ip_type']      = '電池交換站IP類別';
$_['column_ip']           = '電池交換站IP';
$_['column_last_online']  = '最後一次連線回報時';
$_['column_version_no']   = '電池交換站版本號碼';
$_['column_version_date'] = '電池交換站版本日期';
$_['column_status']       = '状态';
$_['column_track_quantity'] = '軌道數';


$_['column_action']      = '動作';

// Entry
$_['entry_username']   	= '帳號';
$_['entry_user_group'] 	= '類別';
$_['entry_password']   	= '密碼';
$_['entry_confirm']    	= '確認密碼';
$_['entry_firstname']  	= '名字';
$_['entry_lastname']   	= '姓氏';
$_['entry_email']      	= '電子郵件';
$_['entry_image']      	= '照片';
$_['entry_status']     	= '狀態';

// Error
$_['error_permission'] 	= '您沒有權限更改使用者的設置!';
$_['error_account']    	= '不能被刪除，這是目前的登入帳號!';
$_['error_exists']     	= '您輸入的帳號已註冊過，請重新輸入!';
$_['error_username']   	= '帳號必須在3到20個字元之間!';
$_['error_password']   	= '密碼必須在4到20個字元之間!';
$_['error_confirm']    	= '密碼和確認密碼不符!';
$_['error_firstname']  	= '名字必須在1到32個字元之間!';
$_['error_lastname']   	= '姓氏必須在1到32個字元之間!';
$_['error_email']           = '錯誤的電子郵件格式!';
$_['error_exists_email']    = '您輸入的電子郵件已註冊過，請重新輸入!';