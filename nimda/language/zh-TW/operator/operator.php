<?php
// Heading
$_['heading_title']     = '营运商管理';

// Text
$_['text_success'] = '更新成功';
$_['text_list']    = '列表';
$_['text_add']     = '新增';
$_['text_edit']    = '編輯';
$_['text_delete']  = '刪除';

// Column
$_['column_top13']       = '营运地區';
$_['column_tas_idx']       = '主機營運商';
$_['column_top01']       = '营运商名称';
$_['column_station_cnt'] = '机柜数';
$_['column_battery_cnt'] = '电池数量';
$_['column_top02']       = '型别';
$_['column_top03']       = '连络人';
$_['column_top04']       = '连络电话';
$_['column_top05']       = '行动电话';
$_['column_status']      = '状态';
$_['column_action']      = '動作';

// Modal
$_['modal_title'] = '营运商管理 编辑' ;

// Entry
$_['entry_username']   	= '帳號';
$_['entry_user_group'] 	= '類別';
$_['entry_password']   	= '密碼';
$_['entry_confirm']    	= '確認密碼';
$_['entry_firstname']  	= '名字';
$_['entry_lastname']   	= '姓氏';
$_['entry_email']      	= '電子郵件';
$_['entry_image']      	= '照片';
$_['entry_status']     	= '狀態';

// Error
$_['error_permission'] 	= '您沒有權限更改使用者的設置!';
$_['error_account']    	= '不能被刪除，這是目前的登入帳號!';
$_['error_exists']     	= '您輸入的帳號已註冊過，請重新輸入!';
$_['error_username']   	= '帳號必須在3到20個字元之間!';
$_['error_password']   	= '密碼必須在4到20個字元之間!';
$_['error_confirm']    	= '密碼和確認密碼不符!';
$_['error_firstname']  	= '名字必須在1到32個字元之間!';
$_['error_lastname']   	= '姓氏必須在1到32個字元之間!';
$_['error_email']           = '錯誤的電子郵件格式!';
$_['error_exists_email']    = '您輸入的電子郵件已註冊過，請重新輸入!';