<?php
// Text
$_['text_dashboard']         = '資訊總覽';
$_['text_system']            = '系統管理';
$_['text_users']             = '權限管理';
$_['text_user']              = '使用者管理';
$_['text_user_group']        = '群組帳號';
$_['text_system_parameters'] = '系統參數管理';
$_['text_system_menu']       = '系統功能管理';

$_['text_host']              = '主機';

$_['text_operation']         = '營運';
$_['text_operator']          = '營運商管理';
$_['text_sub_operator']      = '子營運商管理';
$_['text_exchange_station']  = '交換站管理';
$_['text_host_management']   = '營運主機管理';

$_['text_device']            = '裝置';
$_['text_device_cabinet']    = '機櫃管理';
$_['text_device_track']      = '电池租借轨道管理';
$_['text_device_battery']    = '电池管理';
