<?php
/**
 * [ModelOperatorOperator 营运商管理]
 * @Another Angus
 * @date    2019-11-27
 */
class ModelOperatorOperator extends Model {

	/**
	 * [getUsers description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-11-21
	 */
	public function getList($data = array()) {
		$sort_data = array() ;
		$column = array();
		if ( isset($data['s_num']) && $data['s_num'] != "") {
			$column[] = "s_num='".$this->db->escape($data['s_num'])."'";
		}
		if ( isset($data['top01']) && $data['top01'] != "") {
			$column[] = "top01 like '%".$this->db->escape($data['top01'])."%'";
		}

		$SQLCmd = "SELECT * FROM tbl_operator" ;

		if(count($column)) {
			$SQLCmd .= " WHERE " . implode(" AND ",$column);
		}

		// 預設排序欄位 -----------------------------------------------------------------------
		$sort_data = array(
				"top01",
				"top02",
				"top03",
				"top04",
				"top05",
				"top13",
				"tas_idx",
				"status",
		);
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$SQLCmd .= " ORDER BY " . $data['sort'];
			if (( $data['order'] == 'DESC')) {
				$SQLCmd .= " DESC";
			} else {
				$SQLCmd .= " ASC";
			}
		} else {
			// 預設排序條件
			$SQLCmd .= " ORDER BY status ASC" ;
		}

		// 分頁部份 -------------------------------------------------------------------------
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$SQLCmd .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		// dump( $SQLCmd) ;
		$query = $this->db->query($SQLCmd);
		// dump( $query) ;
		return $query->rows;
	}

	/**
	 * [getTotalUsers description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-11-21
	 */
	public function getTotal() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM tbl_operator") ;

		return $query->row['total'];
	}

	/**
	 * [dashboardCnt description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2020-01-28
	 */
	public function dashboardCnt() {
		$SQLCmd = "SELECT COUNT(*) AS total FROM tbl_operator WHERE status='Y'" ;
		$query = $this->db->query( $SQLCmd) ;
		// dump( $SQLCmd) ;
		return $query->row['total'];
	}

	/**
	 * [getOperator description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-11-28
	 */
	public function getOperator() {
		$retArr = array() ;

		$SQLCmd = "SELECT s_num, top01, status FROM tbl_operator" ;
		$query = $this->db->query( $SQLCmd) ;

		$statusArr = $this->config->get( 'status') ;
		// dump( $statusArr) ;
		// dump( $query->rows) ;
		foreach ($query->rows as $iCnt => $row) {
			if ( $row['status'] == 'N') {
				$strStatus = isset( $statusArr[$row['status']]) ? $statusArr[$row['status']] : "" ;
				$retArr[$row['s_num']] = $row['top01'] ."(". $strStatus . ")" ;
			} else {
				$retArr[$row['s_num']] = $row['top01'] ;
			}
		}
		return $retArr ;
	}

	/**
	 * [editResource description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-11-29
	 */
	public function addResource( $data = array()) {
		$noAllowed = array( 's_num') ;
		$colUpdate = $this->user->markInsertUser() ;
		$s_num = $this->guid->getGUID( false);
		$colUpdate[] = "s_num='" . $s_num . "'" ;

		//同步用
		$noAllowed2 = array('s_num','tas_idx', 'top13') ;
		$colUpdate2 = $this->user->markInsertUser() ;
		$colUpdate2[] = "oc_s_num='" . $s_num . "'" ;

		$tas_idx = "";
		foreach ($data as $keyName => $value) {
			if ( !in_array( $keyName, $noAllowed)) {
				if($keyName == 'tas_idx'){
					$tas_idx = trim($value);
				}
				$colUpdate[] = "{$keyName}='".$this->db->escape(trim( $value))."'" ;
			}

			if ( !in_array( $keyName, $noAllowed2)) {
				$colUpdate2[] = "{$keyName}='".$this->db->escape(trim( $value))."'" ;
			}

		}
		// $colUpdate[] = "" ;

		$setStr = implode( ',', $colUpdate) ;
		$SQLCmd = "INSERT INTO tbl_operator SET {$setStr} " ;
		$rs_flag = $this->db->query( $SQLCmd);
		// dump( $SQLCmd) ;
		//同步insert
		if($rs_flag){
			$setStr2 = implode( ',', $colUpdate2) ;
			$this->syncbackground($tas_idx,$setStr2);
		}
		return $rs_flag;
	}

	/**
	 * [editResource description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-11-29
	 */
	public function editResource( $data = array()) {
		// 排除清單
		$noAllowed = array( "s_num") ;
		$colUpdate = $this->user->markUpdateUser() ;

		//同步用
		$noAllowed2 = array('s_num','tas_idx', 'top13') ;
		$colUpdate2 = $this->user->markInsertUser() ;
		$tas_idx = "";
		foreach ($data as $keyName => $value) {
			if ( !in_array( $keyName, $noAllowed)) {
				if($keyName == 'tas_idx'){
					$tas_idx = trim($value);
				}
				$colUpdate[] = "{$keyName}='".$this->db->escape(trim( $value))."'" ;
			}
			if ( !in_array( $keyName, $noAllowed2)) {
				$colUpdate2[] = "{$keyName}='".$this->db->escape(trim( $value))."'" ;
			}
		}
		$setStr = implode( ',', $colUpdate) ;
		$SQLCmd = "UPDATE tbl_operator SET {$setStr} WHERE s_num='". $this->db->escape($data['s_num'])."'" ;
		$rs_flag = $this->db->query( $SQLCmd) ;
		if($rs_flag){
			//同步
			$setStr2 = implode( ',', $colUpdate2) ;
			$this->syncbackground($tas_idx,$setStr2,$data['s_num']);
		}
		// dump( $SQLCmd) ;
		return $rs_flag ;
	}

	/**
	 * [delResource description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-11-29
	 */
	public function delResource($s_num) {
		$SQLCmd = "UPDATE tbl_operator SET status='D' WHERE s_num='". $this->db->escape($s_num)."'" ;
		$rs_flag = $this->db->query( $SQLCmd) ;
		if($rs_flag){
			//同步
			$this->syncbackground("","",$s_num,"del");
		}

	}

	public function syncbackground($tas_idx, $setStr2, $s_num="", $use_type=""){
		$mysql_hotename= "";
		$mysql_username= "";
		$mysql_password= "";
		$mysql_database = "";

		if($use_type == "del"){
			//查營運商的server
			$SQLCmd = "select
							tas.server_location,tas.db_acc,tas.db_pwd,tas.database_name
						from tbl_operator top
						left join tbl_ap_server tas ON tas.idx = top.tas_idx
						where top.s_num='{$s_num}'";
		}
		if($tas_idx != ""){
			//查營運商的server
			$SQLCmd = "select server_location,db_acc,db_pwd,database_name
						from tbl_ap_server
						where idx='{$tas_idx}'";
		}
		if($SQLCmd != ""){
			$query = $this->db->query($SQLCmd);
			foreach ($query->rows as $iCnt => $row) {
				$mysql_hotename = $row['server_location'];
				$mysql_username = $row['db_acc'];
				$mysql_password = $row['db_pwd'];
				$mysql_database = $row['database_name'];
			}

			if($mysql_hotename != '' && $mysql_username != '' && $mysql_password != '' && $mysql_database != ''){
				$conn2 = mysql_connect($mysql_hotename,$mysql_username,$mysql_password);
				mysql_query("set names 'utf8'");
				mysql_select_db($mysql_database, $conn2);

				if($use_type == "del"){
					$colUpdate2 = $this->user->markDeleteUser() ;
					$setStr2 = implode( ',', $colUpdate2) ;

					$SQLCmd2 = "UPDATE {$mysql_database}.tbl_operator SET status='D', {$setStr2} WHERE oc_s_num='".$this->db->escape($s_num)."'";
				}else{
					if($s_num == ""){
						$SQLCmd2 = "INSERT INTO {$mysql_database}.tbl_operator SET {$setStr2} " ;
					}else{
						$SQLCmd2 = "UPDATE {$mysql_database}.tbl_operator SET {$setStr2} WHERE oc_s_num='".$this->db->escape($s_num)."'";
					}
				}
				mysql_query($SQLCmd2);
			}
		}
	}
}