<?php
/**
 * [ModelOperatorCountry 营运國家/地區]
 * @Another Angus
 * @date    2019-12-22
 */
/**
 * return stdClass Object
 * [num_rows] => 3
 * [row] => Array
 * [rows] => Array(
 *        [0] => Array
 *        [1] => Array
 * )
 */

class ModelOperatorCountry extends Model {

	/**
	 * [getCountryList 取出國家/地區名稱]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-12-22
	 */
	public function getCountryList() {
		$retInfo = array() ;
		$SQLCmd = "SELECT country_id, iso_code_2, iso_code_3, iso_desc FROM sys_country WHERE status=1 AND iso_desc != '' ORDER BY iso_code_2 ASC" ;
		$query = $this->db->query($SQLCmd) ;

		foreach ($query->rows as $key => $row) {
			$retinfo[ $row['country_id']] = $row ;
		}
		return $retinfo ;
	}
}