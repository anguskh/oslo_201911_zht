<?php
/**
 * [ModelOperatorBatterySwapTrack 电池租借轨道管理]
 * @Another Angus
 * @date    2019-11-27
 */
class ModelOperatorBatterySwapTrack extends Model {
	/**
	 * [getUsers description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-11-21
	 */
	public function getConfigs($data = array()) {
		$sort_data = array() ;
		$column = array();
		// if ( isset($data['filter_syear']) && $data['filter_syear'] != "") {
		// 	$column[] = "syear='".$this->db->escape($data['filter_syear'])."'";
		// }

		$SQLCmd = "SELECT * FROM tbl_battery_swap_track" ;

		if(count($column)) {
			$SQLCmd .= " WHERE " . implode(" AND ",$column);
		}

		// 預設排序欄位 -----------------------------------------------------------------------
		$sort_data = array(
				"so_num",
				"sb_num",
				"track_no",
				"column_park",
				"column_charge",
				"battery_id",
				"status",
		);
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$SQLCmd .= " ORDER BY " . $data['sort'];
			if (( $data['order'] == 'DESC')) {
				$SQLCmd .= " DESC";
			} else {
				$SQLCmd .= " ASC";
			}
		} else {
			// 預設排序條件
			$SQLCmd .= " ORDER BY create_date ASC" ;
		}

		// 分頁部份 -------------------------------------------------------------------------
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$SQLCmd .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		// dump( $SQLCmd) ;
		$query = $this->db->query($SQLCmd);
		// dump( $query) ;
		return $query->rows;
	}

	/**
	 * [getTotalUsers description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-11-21
	 */
	public function getTotalConfigs() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM tbl_battery_swap_track") ;

		return $query->row['total'];
	}

}