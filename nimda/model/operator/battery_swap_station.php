<?php
/**
 * [ModelOperatorBatterySwapStation 电池租借站管理]
 * @Another Angus
 * @date    2019-11-26
 */
/**
 * return stdClass Object
 * [num_rows] => 3
 * [row] => Array
 * [rows] => Array(
 *        [0] => Array
 *        [1] => Array
 * )
 */

class ModelOperatorBatterySwapStation extends Model {
	/**
	 * [getUsers description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-11-21
	 */
	public function getBss($data = array()) {
		$sort_data = array() ;
		$column = array();
		if ( isset($data['s_num']) && $data['s_num'] != "") {
			$column[] = "s_num='".$this->db->escape($data['s_num'])."'";
		}

		if ( isset($data['location']) && $data['location'] != "") {
			$column[] = "location like '%".$this->db->escape($data['location'])."%'";
		}

		if ( isset($data['so_num'])) {
			$column[] = "so_num like '%".$this->db->escape($data['so_num'])."%'";
		}

		$SQLCmd = "SELECT * FROM tbl_battery_swap_station" ;

		if(count($column)) {
			$SQLCmd .= " WHERE " . implode(" AND ",$column);
		}

		// 預設排序欄位 -----------------------------------------------------------------------
		$sort_data = array(
			"so_num",
			"location",
			"exchange_num",
			"canuse_battery_num",
		);
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$SQLCmd .= " ORDER BY " . $data['sort'];
			if (( $data['order'] == 'DESC')) {
				$SQLCmd .= " DESC";
			} else {
				$SQLCmd .= " ASC";
			}
		} else {
			// 預設排序條件
			$SQLCmd .= " ORDER BY create_date ASC" ;
		}

		// 分頁部份 -------------------------------------------------------------------------
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$SQLCmd .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		// dump( $SQLCmd) ;
		$query = $this->db->query($SQLCmd);
		// dump( $query) ;
		return $query->rows;
	}

	/**
	 * [getTotalUsers description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-11-21
	 */
	public function getBssTotal() {

		$query = $this->db->query("SELECT COUNT(*) AS total FROM tbl_battery_swap_station") ;
		// dump( "SELECT COUNT(*) AS total FROM tbl_battery_swap_station") ;
		return $query->row['total'];
	}

	/**
	 * [dashboardCnt description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2020-01-28
	 */
	public function dashboardCnt() {
		$SQLCmd = "SELECT COUNT(*) AS total FROM tbl_battery_swap_station WHERE status='Y'" ;
		$query = $this->db->query( $SQLCmd) ;
		// dump( $SQLCmd) ;
		return $query->row['total'];
	}

	/**
	 * [editResource description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-11-29
	 */
	public function addResource( $data = array()) {
		$noAllowed = array( 's_num', 'operator_name') ;
		$colUpdate = $this->user->markInsertUser() ;
		$s_num = $this->guid->getGUID( false);
		$colUpdate[] = "s_num='" . $s_num . "'" ;

		$setStr = implode( ',', $colUpdate) ;
		$SQLCmd = "INSERT INTO tbl_battery_swap_station SET {$setStr} " ;
		// dump( $SQLCmd) ;
		// exit() ;
		return $this->db->query( $SQLCmd) ;
	}

	/**
	 * [editResource description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-11-29
	 */
	public function editResource( $data = array()) {
		// 排除清單
		$noAllowed = array( 's_num', 'operator_name') ;
		$colUpdate = $this->user->markUpdateUser() ;

		$so_num = "";
		foreach ($data as $keyName => $value) {
			if ( !in_array( $keyName, $noAllowed)) {
				$colUpdate[] = "{$keyName}='".$this->db->escape(trim( $value))."'" ;
			}
			if($keyName == "so_num"){
				$so_num = $this->db->escape(trim( $value));
			}else{
				$colUpdate2[] = "{$keyName}='".$this->db->escape(trim( $value))."'" ;
			}
		}
		$setStr = implode( ',', $colUpdate) ;
		$SQLCmd = "UPDATE tbl_battery_swap_station SET {$setStr} WHERE s_num='". $this->db->escape($data['s_num'])."'" ;
		// dump( $SQLCmd) ;
		// exit() ;
		//同步insert
		$rs_flag = $this->db->query( $SQLCmd);
		if($rs_flag){
			$this->syncbackground($so_num,$data['s_num']);
		}
		return $rs_flag ;
	}

	/**
	 * [delResource description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-11-29
	 */
	public function delResource($s_num) {
		$SQLCmd = "UPDATE tbl_battery_swap_station SET status='D' WHERE s_num='". $this->db->escape($s_num)."'" ;
		$rs_flag = $this->db->query( $SQLCmd) ;
		if($rs_flag){
			//同步
			$this->syncbackground("",$s_num,"del");
		}
	}

	/**
	 * [ajaxGetBss description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-12-12
	 */
	public function ajaxGetBss( $data = array()) {
		$column = array();
		if ( isset($data['s_num']) && $data['s_num'] != "") {
			$column[] = "bss.s_num='".$this->db->escape($data['s_num'])."'";
		}

		$SQLCmd = "SELECT bss.*, op.top01 as operator_name
				FROM tbl_battery_swap_station bss
					LEFT JOIN tbl_operator op ON bss.so_num=op.s_num" ;
		if(count($column)) {
			$SQLCmd .= " WHERE " . implode(" AND ",$column);
		}
		$query = $this->db->query($SQLCmd);
		// dump( $query) ;
		return $query->rows;
	}

	public function getStation() {
		$retArr = array() ;

		$SQLCmd = "SELECT s_num, location, status FROM tbl_battery_swap_station" ;
		$query = $this->db->query( $SQLCmd) ;

		$statusArr = $this->config->get( 'status') ;
		// dump( $statusArr) ;
		// dump( $query->rows) ;
		foreach ($query->rows as $iCnt => $row) {
			if ( $row['status'] == 'N') {
				$strStatus = isset( $statusArr[$row['status']]) ? $statusArr[$row['status']] : "" ;
				$retArr[$row['s_num']] = $row['location'] ."(". $strStatus . ")" ;
			} else {
				$retArr[$row['s_num']] = $row['location'] ;
			}
		}
		return $retArr ;
	}

	public function syncbackground($so_num="",$s_num="",$use_type=""){
		//先查詢營運商的主機
		$mysql_hotename= "";
		$mysql_username= "";
		$mysql_password= "";
		$mysql_database = "";
		//查營運商的server
		if($use_type=="del"){
			$SQLCmd = "select
							tas.server_location,tas.db_acc,tas.db_pwd,tas.database_name
						from tbl_battery_swap_station tbss
						left join tbl_operator top on top.s_num = tbss.so_num
						left join tbl_ap_server tas ON tas.idx = top.tas_idx
						where tbss.s_num='{$s_num}'";
		}else{
			$SQLCmd = "select
							tas.server_location,tas.db_acc,tas.db_pwd,tas.database_name
						from tbl_operator top
						left join tbl_ap_server tas ON tas.idx = top.tas_idx
						where top.s_num='{$so_num}'";
		}
		$query = $this->db->query($SQLCmd);
		foreach ($query->rows as $iCnt => $row) {
			$mysql_hotename = $row['server_location'];
			$mysql_username = $row['db_acc'];
			$mysql_password = $row['db_pwd'];
			$mysql_database = $row['database_name'];
		}

		if($mysql_hotename != '' && $mysql_username != '' && $mysql_password != '' && $mysql_database != ''){
			$conn2 = mysql_connect($mysql_hotename,$mysql_username,$mysql_password);
			mysql_query("set names 'utf8'");
			mysql_select_db($mysql_database, $conn2);

			//先檢查$so_num在後台的s_num
			$sql = "select s_num from {$mysql_database}.tbl_operator where oc_s_num = '{$so_num}'";
			$query = $this->db->query($sql);
			$so_num_new = "";
			foreach ($query->rows as $iCnt => $row) {
				$so_num_new = $row['s_num'];
			}
			$keyNamearr = array('location', 'province', 'city', 'district', 'status') ;
			//撈出交換站底下的機櫃 要一併寫入營運商後台的交換站管理
			$SQLCmd = "select tbsc.s_num,tbss.location,tbss.note,tbss.province,tbss.city,tbss.district,tbss.status,
						tbsc.note, tbsc.bss_id, tbsc.bss_token, tbsc.ip_type, tbsc.ip, tbsc.version_no, tbsc.version_date, tbsc.track_quantity,tbsc.station_no,tbsc.sim_no
						from tbl_battery_swap_station tbss
						left join tbl_battery_swap_cabinet tbsc ON tbsc.ss_num = tbss.s_num
						where tbss.s_num='{$s_num}'";
			$query = $this->db->query($SQLCmd);
			foreach ($query->rows as $iCnt => $row) {
				$colUpdate2 = array();
				//刪除
				if($use_type=="del"){
					$colUpdate2 = $this->user->markDeleteUser() ;
					$setStr2 = implode( ',', $colUpdate2) ;
					$SQLCmd2 = "UPDATE {$mysql_database}.tbl_battery_swap_station SET status='D',{$setStr2} WHERE oc_s_num='".$row['s_num']."'";
					mysql_query($SQLCmd2);
				}else{
					//更新
					$colUpdate2 = $this->user->markUpdateUser() ;
					$colUpdate2[] = "so_num='".$this->db->escape($so_num_new)."'";
					foreach($keyNamearr as $arr){
						if(isset($row[$arr])){
							$colUpdate2[] = "{$arr}='".$this->db->escape($row[$arr])."'";
						}
					}

					$setStr2 = implode( ',', $colUpdate2) ;
					$SQLCmd2 = "UPDATE {$mysql_database}.tbl_battery_swap_station SET {$setStr2} WHERE oc_s_num='".$row['s_num']."'";
					mysql_query($SQLCmd2);
				}
			}
		}
	}
}

