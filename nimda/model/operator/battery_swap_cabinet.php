<?php
/**
 * [ModelOperatorBatterySwapCabinet 機櫃管理]
 * @Another Angus
 * @date    2019-12-01
 */
class ModelOperatorBatterySwapCabinet extends Model {
	/**
	 * [getUsers description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-12-01
	 */
	public function getBSC($data = array()) {
		$sort_data = array() ;
		$column = array();
		// if ( isset($data['filter_syear']) && $data['filter_syear'] != "") {
		// 	$column[] = "syear='".$this->db->escape($data['filter_syear'])."'";
		// }

		$SQLCmd = "SELECT * FROM tbl_battery_swap_cabinet" ;

		if(count($column)) {
			$SQLCmd .= " WHERE " . implode(" AND ",$column);
		}

		// 預設排序欄位 -----------------------------------------------------------------------
		$sort_data = array(
			"bss_no",
			"bss_id",
			"ip_type",
			"ip",
			"last_online",
			"version_no",
			"version_date",
			"status",
		);
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$SQLCmd .= " ORDER BY " . $data['sort'];
			if (( $data['order'] == 'DESC')) {
				$SQLCmd .= " DESC";
			} else {
				$SQLCmd .= " ASC";
			}
		} else {
			// 預設排序條件
			$SQLCmd .= " ORDER BY create_date ASC" ;
		}

		// 分頁部份 -------------------------------------------------------------------------
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$SQLCmd .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		// dump( $SQLCmd) ;
		$query = $this->db->query($SQLCmd);
		// dump( $query) ;
		return $query->rows;
	}
	/**
	 * [editResource description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-11-29
	 */
	public function addResource( $data = array()) {
		$noAllowed = array( 's_num', 'operator_name', 'station_name') ;
		$colUpdate = $this->user->markInsertUser() ;
		$s_num = $this->guid->getGUID( false);
		$colUpdate[] = "s_num='" . $s_num . "'" ;

		//同步用
		$noAllowed2 = array('s_num', 'so_num','ss_num', 'operator_name', 'station_name') ;
		$colUpdate2 = $this->user->markInsertUser() ;
		$colUpdate2[] = "oc_s_num='" . $s_num . "'" ;

		$tas_idx = "";
		$ss_num = "";
		foreach ($data as $keyName => $value) {
			if ( !in_array( $keyName, $noAllowed)) {
				if($keyName == 'so_num'){
					$tas_idx = trim($value);
				}
				if($keyName == 'ss_num'){
					$ss_num = trim($value);
				}

				$colUpdate[] = "{$keyName}='".$this->db->escape(trim( $value))."'" ;
			}

			if ( !in_array( $keyName, $noAllowed2)) {
				$colUpdate2[] = "{$keyName}='".$this->db->escape(trim( $value))."'" ;
			}
		}

		// $colUpdate[] = "" ;

		$setStr = implode( ',', $colUpdate) ;
		$SQLCmd = "INSERT INTO tbl_battery_swap_cabinet SET {$setStr} " ;
		// dump( $SQLCmd) ;
		// exit() ;
		$rs_flag = $this->db->query( $SQLCmd);
		//同步insert
		//$rs_flag = 1;
		if($rs_flag){
			$this->syncbackground($tas_idx,$colUpdate2,$s_num="",$use_type="",$ss_num);
		}
		return $rs_flag;
	}

	/**
	 * [editResource description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-11-29
	 */
	public function editResource( $data = array()) {
		// 排除清單
		$noAllowed = array( 's_num', 'operator_name', 'station_name') ;
		$colUpdate = $this->user->markUpdateUser() ;

		//同步用
		$noAllowed2 = array('s_num', 'so_num','ss_num', 'operator_name', 'station_name') ;
		$colUpdate2 = $this->user->markUpdateUser() ;
		$colUpdate2[] = "oc_s_num='" . $data['s_num'] . "'" ;

		$tas_idx = "";
		$ss_num = "";
		foreach ($data as $keyName => $value) {
			if ( !in_array( $keyName, $noAllowed)) {
				if($keyName == 'so_num'){
					$tas_idx = trim($value);
				}
				if($keyName == 'ss_num'){
					$ss_num = trim($value);
				}

				$colUpdate[] = "{$keyName}='".$this->db->escape(trim( $value))."'" ;
			}

			if ( !in_array( $keyName, $noAllowed2)) {
				$colUpdate2[] = "{$keyName}='".$this->db->escape(trim( $value))."'" ;
			}
		}

		$setStr = implode( ',', $colUpdate) ;
		$SQLCmd = "UPDATE tbl_battery_swap_cabinet SET {$setStr} WHERE s_num='". $this->db->escape($data['s_num'])."'" ;
		// dump( $SQLCmd) ;
		// exit() ;
		$rs_flag = $this->db->query( $SQLCmd);
		//同步insert
		//$rs_flag = 1;
		if($rs_flag){
			$this->syncbackground($tas_idx,$colUpdate2,$data['s_num'],$use_type="",$ss_num);
		}
		return $rs_flag;
	}

	/**
	 * [delResource description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-11-29
	 */
	public function delResource($s_num) {
		$SQLCmd = "UPDATE tbl_battery_swap_cabinet SET status='D' WHERE s_num='". $this->db->escape($s_num)."'" ;
		$rs_flag = $this->db->query( $SQLCmd) ;
		if($rs_flag){
			//同步
			$this->syncbackground("","",$s_num,"del");
		}

	}

	/**
	 * [getTotalUsers description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-12-01
	 */
	public function getTotalBSC() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM tbl_battery_swap_cabinet") ;

		return $query->row['total'];
	}

	/**
	 * [dashboardCnt description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2020-01-28
	 */
	public function dashboardCnt() {
		$SQLCmd = "SELECT COUNT(*) AS total FROM tbl_battery_swap_cabinet WHERE status='Y'" ;
		$query = $this->db->query( $SQLCmd) ;
		// dump( $SQLCmd) ;
		return $query->row['total'];
	}

	public function ajaxGetBSC( $data = array()) {
		$column = array();
		if ( isset($data['s_num']) && $data['s_num'] != "") {
			$column[] = "tbsc.s_num='".$this->db->escape($data['s_num'])."'";
		}

		$SQLCmd = "SELECT tbsc.*, bss.location as station_name, op.top01 as operator_name
				FROM tbl_battery_swap_cabinet tbsc
					LEFT JOIN tbl_battery_swap_station bss ON bss.s_num = tbsc.ss_num
					LEFT JOIN tbl_operator op ON tbsc.so_num=op.s_num" ;
		if(count($column)) {
			$SQLCmd .= " WHERE " . implode(" AND ",$column);
		}
		$query = $this->db->query($SQLCmd);
		// dump( $query) ;
		return $query->rows;
	}

	public function syncbackground($tas_idx,$colUpdate2,$s_num="",$use_type="", $ss_num = ""){
		$mysql_hotename= "";
		$mysql_username= "";
		$mysql_password= "";
		$mysql_database = "";
		//查營運商的server
		if($use_type=="del"){
			$SQLCmd = "select
							tas.server_location,tas.db_acc,tas.db_pwd,tas.database_name
						from tbl_battery_swap_cabinet tbsc
						left join tbl_operator top on top.s_num = tbsc.so_num
						left join tbl_ap_server tas ON tas.idx = top.tas_idx
						where tbsc.s_num='{$s_num}'";
		}else{
			$SQLCmd = "select
							tas.server_location,tas.db_acc,tas.db_pwd,tas.database_name
						from tbl_operator top
						left join tbl_ap_server tas ON tas.idx = top.tas_idx
						where top.s_num='{$tas_idx}'";
		}
		if($SQLCmd != ""){
			$query = $this->db->query($SQLCmd);
			foreach ($query->rows as $iCnt => $row) {
				$mysql_hotename = $row['server_location'];
				$mysql_username = $row['db_acc'];
				$mysql_password = $row['db_pwd'];
				$mysql_database = $row['database_name'];
			}

			if($mysql_hotename != '' && $mysql_username != '' && $mysql_password != '' && $mysql_database != ''){
				$conn2 = mysql_connect($mysql_hotename,$mysql_username,$mysql_password);
				mysql_query("set names 'utf8'");
				mysql_select_db($mysql_database, $conn2);

				//檢查目前交換站的資料
				if($use_type == "del"){
					$colUpdate2 = $this->user->markDeleteUser() ;
					$setStr2 = implode( ',', $colUpdate2) ;

					$SQLCmd2 = "UPDATE {$mysql_database}.tbl_battery_swap_station SET status='D', {$setStr2} WHERE oc_s_num='".$this->db->escape($s_num)."'";
				}else{
					if($ss_num != ''){
						$keyNamearr = array('location', 'province', 'city', 'district', 'status') ;
						$SQLCmd = "select location,so_num,note,province,city,district,status
									from tbl_battery_swap_station
									where s_num='{$ss_num}'";
						$query = $this->db->query($SQLCmd);
						$so_num = "";
						foreach ($query->rows as $iCnt => $row) {
							foreach($keyNamearr as $arr){
								if(isset($row[$arr])){
									$colUpdate2[] = "{$arr}='".$this->db->escape($row[$arr])."'";
								}
							}
							$so_num = $row['so_num'];
						}


						if($so_num != ""){
							//先檢查$so_num在後台的s_num
							$sql = "select s_num from {$mysql_database}.tbl_operator where oc_s_num = '{$so_num}'";
							$query = $this->db->query($sql);
							$so_num_new = "";
							foreach ($query->rows as $iCnt => $row) {
								$so_num_new = $row['s_num'];
							}
							if($so_num_new != ""){
								$colUpdate2[] = "so_num={$so_num_new}";
							}
						}

						$setStr2 = implode( ',', $colUpdate2) ;
						if($s_num == ""){
							//先檢查是否有營運商 沒有的話要建檔營運商
							$SQLCmd2 = "INSERT INTO {$mysql_database}.tbl_battery_swap_station SET {$setStr2} " ;
						}else{
							$SQLCmd2 = "UPDATE {$mysql_database}.tbl_battery_swap_station SET {$setStr2} WHERE oc_s_num='".$this->db->escape($s_num)."'";
						}

					}
				}
				mysql_query($SQLCmd2);
			}
		}
	}

	//查出是否有重複
	public function countbss_id($bss_id, $s_num){
		$where = "";
		if($bss_id != ''){
			if($s_num != ''){
				$where = " and tbss.s_num != '{$s_num}'";
			}
			$SQLCmd = "	select count(*) as cnt
						from tbl_battery_swap_cabinet tbss 
						where tbss.bss_id = '{$bss_id}' and tbss.status <> 'D' {$where}";
			$query = $this->db->query($SQLCmd);
			$rs = $query->rows;
			if($rs[0]['cnt'] == 0){  //無資料
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

}