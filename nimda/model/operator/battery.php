<?php
/**
 * [ModelOperatorBattery 电池管理]
 * @Another Angus
 * @date    2019-11-26
 */
class ModelOperatorBattery extends Model {
	/**
	 * [getUsers description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-11-21
	 */
	public function getList($data = array()) {
		$sort_data = array() ;
		$column = array();
		// if ( isset($data['filter_syear']) && $data['filter_syear'] != "") {
		// 	$column[] = "syear='".$this->db->escape($data['filter_syear'])."'";
		// }

		if ( isset($data['so_num'])) {
			$column[] = "do_num like '%".$this->db->escape($data['do_num'])."%'";
		}

		$SQLCmd = "SELECT * FROM tbl_battery" ;

		if(count($column)) {
			$SQLCmd .= " WHERE " . implode(" AND ",$column);
		}

		// 預設排序欄位 -----------------------------------------------------------------------
		$sort_data = array(
			"battery_id",
			"DorO_flag",
			"do_num",
			"sv_num",
			"manufacture_date",
			"position",
			"map",
			"exchange_count",
			"last_report_datetime",
			"station",
			"abc",
			"history",
			"SOC",
			"SOH",
			"status",
			"battery_station_position",
			"battery_gps_manufacturer",
		);
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$SQLCmd .= " ORDER BY " . $data['sort'];
			if (( $data['order'] == 'DESC')) {
				$SQLCmd .= " DESC";
			} else {
				$SQLCmd .= " ASC";
			}
		} else {
			// 預設排序條件
			$SQLCmd .= " ORDER BY manufacture_date ASC" ;
		}

		// 分頁部份 -------------------------------------------------------------------------
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$SQLCmd .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		// dump( $SQLCmd) ;
		$query = $this->db->query($SQLCmd);
		// dump( $query) ;
		return $query->rows;
	}

	/**
	 * [getTotalUsers description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-11-21
	 */
	public function getTotal() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM tbl_battery") ;

		return $query->row['total'];
	}

	/**
	 * [dashboardCnt description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2020-01-28
	 */
	public function dashboardCnt() {
		$SQLCmd = "SELECT COUNT(*) AS total FROM tbl_battery WHERE status in (0, 1)" ;
		$query = $this->db->query( $SQLCmd) ;
		// dump( $SQLCmd) ;
		return $query->row['total'];
	}

	/**
	 * [getBatteryCnt description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-11-28
	 */
	public function getBatteryCnt() {
		$retArr = array() ;
		$SQLCmd = "SELECT do_num, count( do_num) cnt FROM tbl_battery WHERE status IN ('0','1','2','3','4','5')  AND DorO_flag='O' GROUP BY do_num" ;
		$query = $this->db->query( $SQLCmd) ;
		foreach ($query->rows as $iCnt => $row) {
			$retArr[$row['do_num']] = $row['cnt'] ;
		}
		return $retArr ;
	}

	public function ajaxGetBattery( $data = array()) {
		$column = array();
		if ( isset($data['s_num']) && $data['s_num'] != "") {
			$column[] = "tb.s_num='".$this->db->escape($data['s_num'])."'";
		}

		$SQLCmd = "SELECT tb.*, op.top01 as operator_name
				FROM tbl_battery tb
					LEFT JOIN tbl_operator op ON tb.do_num=op.s_num" ;
		if(count($column)) {
			$SQLCmd .= " WHERE " . implode(" AND ",$column);
		}
		$query = $this->db->query($SQLCmd);
		// dump( $query) ;
		return $query->rows;
	}


	/**
	 * [editResource description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-11-29
	 */
	public function addResource( $data = array()) {
		$noAllowed = array( 's_num', 'operator_name') ;
		$colUpdate = $this->user->markInsertUser() ;
		$s_num = $this->guid->getGUID( false);
		$colUpdate[] = "s_num='" . $s_num . "'" ;
		

		//同步用
		$noAllowed2 = array('s_num', 'operator_name', 'do_num') ;
		$colUpdate2 = $this->user->markInsertUser() ;
		$colUpdate2[] = "oc_s_num='" . $s_num . "'" ;

		foreach ($data as $keyName => $value) {
			if ( !in_array( $keyName, $noAllowed)) {
				$colUpdate[] = "{$keyName}='".$this->db->escape(trim( $value))."'" ;
			}

			if ( !in_array( $keyName, $noAllowed2)) {
				$colUpdate2[] = "{$keyName}='".$this->db->escape(trim( $value))."'" ;
			}
		}

		$colUpdate[] = "DorO_flag='O'";//預設O營運商
		$colUpdate2[] = "DorO_flag='O'";//預設O營運商

		$setStr = implode( ',', $colUpdate) ;
		$SQLCmd = "INSERT INTO tbl_battery SET {$setStr} " ;
		$rs_flag = $this->db->query( $SQLCmd);
		// dump( $SQLCmd) ;
		//同步insert
		if($rs_flag){
			$this->syncbackground($data['do_num'],$colUpdate2);
		}
		return $rs_flag;
	}

	/**
	 * [editResource description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-11-29
	 */
	public function editResource( $data = array()) {
		// 排除清單
		$noAllowed = array( 's_num', 'operator_name') ;
		$colUpdate = $this->user->markUpdateUser() ;

		//同步用
		$noAllowed2 = array('s_num', 'operator_name', 'do_num') ;
		$colUpdate2 = $this->user->markUpdateUser() ;

		$so_num = "";
		foreach ($data as $keyName => $value) {
			if ( !in_array( $keyName, $noAllowed)) {
				$colUpdate[] = "{$keyName}='".$this->db->escape(trim( $value))."'" ;
			}
			if ( !in_array( $keyName, $noAllowed2)) {
				$colUpdate2[] = "{$keyName}='".$this->db->escape(trim( $value))."'" ;
			}
		}
		$setStr = implode( ',', $colUpdate) ;
		$SQLCmd = "UPDATE tbl_battery SET {$setStr} WHERE s_num='". $this->db->escape($data['s_num'])."'" ;
		// dump( $SQLCmd) ;
		// exit() ;
		//同步insert
		$rs_flag = $this->db->query( $SQLCmd);
		if($rs_flag){
			$this->syncbackground($data['do_num'],$colUpdate2,$data['s_num']);
		}
		return $rs_flag ;
	}

	/**
	 * [delResource description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-11-29
	 */
	public function delResource($s_num) {
		$SQLCmd = "UPDATE tbl_battery SET status='D' WHERE s_num='". $this->db->escape($s_num)."'" ;
		$rs_flag = $this->db->query( $SQLCmd) ;
		if($rs_flag){
			//同步
			$this->syncbackground("","",$s_num,"del");
		}
	}

	public function syncbackground($so_num="",$colUpdate2="",$s_num="",$use_type=""){
		//先查詢營運商的主機
		$mysql_hotename= "";
		$mysql_username= "";
		$mysql_password= "";
		$mysql_database = "";
		//查營運商的server
		if($use_type=="del"){
			$SQLCmd = "select
							tas.server_location,tas.db_acc,tas.db_pwd,tas.database_name
						from tbl_battery tb
						left join tbl_operator top on top.s_num = tb.do_num
						left join tbl_ap_server tas ON tas.idx = top.tas_idx
						where tb.s_num='{$s_num}'";
		}else{
			$SQLCmd = "select
							tas.server_location,tas.db_acc,tas.db_pwd,tas.database_name
						from tbl_operator top
						left join tbl_ap_server tas ON tas.idx = top.tas_idx
						where top.s_num='{$so_num}'";
		}
		$query = $this->db->query($SQLCmd);
		foreach ($query->rows as $iCnt => $row) {
			$mysql_hotename = $row['server_location'];
			$mysql_username = $row['db_acc'];
			$mysql_password = $row['db_pwd'];
			$mysql_database = $row['database_name'];
		}

		if($mysql_hotename != '' && $mysql_username != '' && $mysql_password != '' && $mysql_database != ''){
			$conn2 = mysql_connect($mysql_hotename,$mysql_username,$mysql_password);
			mysql_query("set names 'utf8'");
			mysql_select_db($mysql_database, $conn2);

			//先檢查$so_num在後台的s_num
			$sql = "select s_num from {$mysql_database}.tbl_operator where oc_s_num = '{$so_num}'";
			$query = $this->db->query($sql);
			$so_num_new = "";
			foreach ($query->rows as $iCnt => $row) {
				$so_num_new = $row['s_num'];
			}
			if($so_num_new != ''){
				$colUpdate2[] = " do_num = ".$so_num_new;
			}

			//將電池管理同步寫入後台
			if($use_type == "del"){
				$colUpdate2 = $this->user->markDeleteUser() ;
				$setStr2 = implode( ',', $colUpdate2) ;

				$SQLCmd2 = "UPDATE {$mysql_database}.tbl_battery SET status='D', {$setStr2} WHERE oc_s_num='".$this->db->escape($s_num)."'";
			}else{
				$setStr2 = implode( ',', $colUpdate2) ;
				if($s_num == ""){
					$SQLCmd2 = "INSERT INTO {$mysql_database}.tbl_battery SET {$setStr2} " ;
				}else{
					$SQLCmd2 = "UPDATE {$mysql_database}.tbl_battery SET {$setStr2} WHERE oc_s_num='".$this->db->escape($s_num)."'";
				}
			}
			mysql_query($SQLCmd2);

		}
	}


	//查出是否有重複
	public function countbattery_id($battery_id, $s_num){
		$where = "";
		if($battery_id != ''){
			if($s_num != ''){
				$where = " and tbss.s_num != '{$s_num}'";
			}
			$SQLCmd = "	select count(*) as cnt
						from tbl_battery tbss 
						where tbss.battery_id = '{$battery_id}' and tbss.status <> 'D' {$where}";
			$query = $this->db->query($SQLCmd);
			$rs = $query->rows;
			if($rs[0]['cnt'] == 0){  //無資料
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

}