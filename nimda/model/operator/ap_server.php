<?php
/**
 * [ModelOperatorApServer 營運主機管理]
 * @Another Angus
 * @date    2019-11-30
 */
class ModelOperatorApServer extends Model {
	/**
	 * [getUsers description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-11-30
	 */
	public function getOperatorApServer($data = array()) {
		$sort_data = array() ;
		$column = array();
		if ( isset($data['idx']) && $data['idx'] != "") {
			$column[] = "idx='".$this->db->escape($data['idx'])."'";
		}

		$SQLCmd = "SELECT * FROM tbl_ap_server" ;

		if(count($column)) {
			$SQLCmd .= " WHERE " . implode(" AND ",$column);
		}

		// 預設排序欄位 -----------------------------------------------------------------------
		$sort_data = array(
			"cid",
			"config_desc",
			"config_set",
			"after_desc",
		);
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$SQLCmd .= " ORDER BY " . $data['sort'];
			if (( $data['order'] == 'DESC')) {
				$SQLCmd .= " DESC";
			} else {
				$SQLCmd .= " ASC";
			}
		} else {
			// 預設排序條件
			$SQLCmd .= " ORDER BY create_date ASC" ;
		}

		// 分頁部份 -------------------------------------------------------------------------
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$SQLCmd .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		// dump( $SQLCmd) ;
		$query = $this->db->query($SQLCmd);
		// dump( $query) ;
		return $query->rows;
	}

	/**
	 * [getTotalUsers description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-11-30
	 */
	public function getTotalOperatorApServer() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM tbl_ap_server") ;

		return $query->row['total'];
	}

	/**
	 * [editResource description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-11-29
	 */
	public function addResource( $data = array()) {
		$noAllowed   = array( 'idx') ;
		$colUpdate   = $this->user->markInsertUser() ;
		$colUpdate[] = "idx='" . $this->guid->getGUID( false) . "'" ;

		foreach ($data as $keyName => $value) {
			if ( !in_array( $keyName, $noAllowed)) {
				$colUpdate[] = "{$keyName}='".$this->db->escape(trim( $value))."'" ;
			}
		}

		$setStr = implode( ',', $colUpdate) ;
		$SQLCmd = "INSERT INTO tbl_ap_server SET {$setStr} " ;
		return $this->db->query( $SQLCmd) ;
	}


	/**
	 * [editResource description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-11-29
	 */
	public function editResource( $data = array()) {
		// 排除清單
		$noAllowed = array( 'idx') ;
		$colUpdate = $this->user->markUpdateUser() ;

		foreach ($data as $keyName => $value) {
			if ( !in_array( $keyName, $noAllowed)) {
				$colUpdate[] = "{$keyName}='".$this->db->escape(trim( $value))."'" ;
			}
		}



		$setStr = implode( ',', $colUpdate) ;
		$SQLCmd = "UPDATE tbl_ap_server SET {$setStr} WHERE idx='". $this->db->escape($data['idx'])."'" ;
		// dump( $SQLCmd) ;
		return $this->db->query( $SQLCmd) ;
	}

	public function getApServerList(){
		$apserverInfo = array();
		$SQLCmd = "SELECT * FROM tbl_ap_server" ;
		$query = $this->db->query($SQLCmd);
		// dump( $query) ;
		foreach ($query->rows as $key => $row) {
			$apserverInfo[ $row['idx']] = $row ;
		}
		return $apserverInfo ;
	}
}