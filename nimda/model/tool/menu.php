<?php
class ModelToolMenu extends Model {

	public function getMenuStructure( ) {
		$menuParent = array() ;
		$menuInfo = array() ;

		$SQLCmd = "SELECT * FROM sys_backend_menu order by seq asc, idx asc" ;
 		$query = $this->db->query( $SQLCmd) ;

 		foreach ($query->rows as $cnt => $row) {
 			$menuParent[ $row['parent']][] = $row['idx'] ;
 			$menuInfo[$row['idx']] = $row ;
 		}
 		ksort( $menuParent) ;

 		return array( $menuParent, $menuInfo) ;
	}
}
