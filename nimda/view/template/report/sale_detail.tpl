<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a href="<?php echo $back; ?>" data-toggle="tooltip" title="<?php echo $button_back; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td width="20%" class="text-center"><?php echo $column_album; ?></td>
                                <td class="text-center"><?php echo $column_product; ?></td>
                                <td width="10%" class="text-center"><?php echo $column_sale; ?></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            if(count($results)){
                                foreach($results as $album => $value){
                                    if(count($value["products"])>1){
                                        echo '<tr>';
                                        echo '<td class="text-center" rowspan="'.count($value["products"]).'">'.$value["name"].'</td>';
                                        echo '<td class="text-left"><img src="'.$value["products"][0]["image"].'" class="img-thumbnail"/></td>';
                                        echo '<td class="text-center">'.$value["products"][0]["total"].'</td>';
                                        echo '</tr>';
                                        $total = 0;
                                        foreach($value["products"] as $k => $v){
                                            $total += $v["total"];
                                            if($k==0)
                                                continue;
                                            echo '<tr>';
                                            echo '<td class="text-left"><img src="'.$v["image"].'" class="img-thumbnail"/></td>';
                                            echo '<td class="text-center">'.$v["total"].'</td>';
                                            echo '</tr>';
                                        }
                                        echo '<tr>';
                                        echo '<th class="text-right bg-danger" scope="row" colspan="2">總計</th>';
                                        echo '<td class="text-center bg-danger">'.$total.'</td>';
                                        echo '</tr>';
                                    }
                                    else{
                                        echo '<tr>';
                                        echo '<td class="text-center">'.$value["name"].'</td>';
                                        echo '<td class="text-left"><img src="'.$value["products"][0]["image"].'" class="img-thumbnail"/></td>';
                                        echo '<td class="text-center">'.$value["products"][0]["total"].'</td>';
                                        echo '</tr>';
                                    }
                                }
                            }
                            ?>
                            <?php if(!count($results)){?>
                            <tr>
                                <td class="text-center" colspan="3"><?php echo $text_no_results;?></td>
                            </tr>
                            <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript"></script> 
</div>
<?php echo $footer; ?>