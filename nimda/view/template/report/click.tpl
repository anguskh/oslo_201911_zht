<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
        <h1><?php echo $heading_title; ?></h1>
        <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
        </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <div class="well">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label" for="input-sdate"><?php echo $entry_date_start; ?></label>
                                <div class="input-group date">
                                    <input type="text" name="sdate" value="<?php echo $sdate; ?>" placeholder="<?php echo $entry_date_start; ?>" data-date-format="YYYY-MM-DD" id="input-sdate" class="form-control" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-edate"><?php echo $entry_date_end; ?></label>
                                <div class="input-group date">
                                    <input type="text" name="edate" value="<?php echo $edate; ?>" placeholder="<?php echo $entry_date_end; ?>" data-date-format="YYYY-MM-DD" id="input-edate" class="form-control" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label" for="input-type"><?php echo $entry_search_type; ?></label>
                                <select name="search_type" class="form-control" id="input-type">
                                    <option value="1" <?php if($search_type==1){?>selected<?php }?>>相簿</option>
                                    <option value="2" <?php if($search_type==2){?>selected<?php }?>>相片</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-value"><?php echo $entry_search_value; ?></label>
                                <input type="text" name="search_value" value="<?php echo $search_value; ?>" placeholder="請輸入相簿名稱或相片編號" id="input-value" class="form-control" />
                            </div>
                            <button type="button" id="button-clear" class="btn btn-danger pull-right" style="margin-left:5px;"><i class="fa fa-eraser"></i> <?php echo $button_clear; ?></button>
                            <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-filter"></i> <?php echo $button_filter; ?></button>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <td width="8%" class="text-center"><?php echo $column_sno; ?></td>
                                <td width="15%" class="text-center"><?php echo $column_album; ?></td>
                                <?php if($search_type==2){?>
                                <td width="10%" class="text-center"><?php echo $column_product_id; ?></td>
                                <td class="text-left"><?php echo $column_product; ?></td>
                                <?php }?>
                                <td width="10%" class="text-center"><?php echo $column_click; ?></td>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if ($clicks) { ?>
                        <?php foreach ($clicks as $click) { ?>
                        <tr>
                            <td class="text-center"><?php echo $click['sno']; ?></td>
                            <td class="text-center"><?php echo $click['album']; ?></td>
                            <?php if($search_type==2){?>
                            <td class="text-center"><?php echo $click['product_id']; ?></td>
                            <td class="text-left">
                                <img src="<?php echo $click['image']; ?>" class="img-thumbnail" />
                            </td>
                            <?php }?>
                            <td class="text-center">
                                <?php if($search_type==2){?>
                                <a href="<?php echo ($click["detail"].'&pid='.$click["product_id"]);?>"><?php echo $click['count']; ?></a>
                                <?php }else{?>
                                <a href="<?php echo ($click["detail"].'&aid='.$click["album_id"]);?>"><?php echo $click['count']; ?></a>
                                <?php }?>
                            </td>
                        </tr>
                        <?php } ?>
                        <?php } else { ?>
                        <tr>
                            <td class="text-center" colspan="5"><?php echo $text_no_results; ?></td>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
$('.date').datetimepicker({
	pickTime: false
});
$('#button-filter').on('click', function() {
	url = 'index.php?route=report/click&token=<?php echo $token; ?>';
	
	var sdate = $('input[name=\'sdate\']').val();
	if (sdate) {
		url += '&sdate=' + encodeURIComponent(sdate);
	}

	var edate = $('input[name=\'edate\']').val();
	if (edate) {
		url += '&edate=' + encodeURIComponent(edate);
	}
    
    var search_type = $('[name=\'search_type\']').val();
    if (search_type) {
		url += '&search_type=' + encodeURIComponent(search_type);
	}
    
    var search_value = $('input[name=\'search_value\']').val();
	if (search_value) {
		url += '&search_value=' + encodeURIComponent(search_value);
	}
	location = url;
});
$("#button-clear").click(function(){
    $('[name="sdate"]').val('');
    $('[name="edate"]').val('');
    $('[name="search_type"]').val('');
    $('[name="search_value"]').val('');
});
</script> 
</div>
<?php echo $footer; ?>