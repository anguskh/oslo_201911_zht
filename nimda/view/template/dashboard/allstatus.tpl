<div class="col-lg-3 col-md-4 col-sm-6">
	<div class="tile">
		<div class="tile-heading">電池交換總次數</div>
		<div class="tile-body"><i class="fa fa-users"></i>
			<h3 class="pull-right" style="font-size: 30px;"> 46,444 次</h3>
		</div>
		<div class="tile-footer"><a href="#">檢視明細...<?php //echo $text_view; ?></a></div>
	</div>
</div>
<div class="col-lg-3 col-md-4 col-sm-6">
	<div class="tile">
		<div class="tile-heading">機車行駛總里程</div>
		<div class="tile-body"><i class="fa fa-users"></i>
			<h3 class="pull-right" style="font-size: 30px;">0 Km</h3>
		</div>
		<div class="tile-footer"><a href="#">檢視明細...<?php //echo $text_view; ?></a></div>
	</div>
</div>
<div class="col-lg-3 col-md-4 col-sm-6">
	<div class="tile">
		<div class="tile-heading">緣農減碳總單位</div>
		<div class="tile-body"><i class="fa fa-users"></i>
			<h3 class="pull-right" style="font-size: 30px;">0 Kg</h3>
		</div>
		<div class="tile-footer"><a href="#">檢視明細...<?php //echo $text_view; ?></a></div>
	</div>
</div><div class="col-lg-3 col-md-4 col-sm-6">
	<div class="tile">
		<div class="tile-heading">換電支付總金額</div>
		<div class="tile-body"><i class="fa fa-users"></i>
			<h3 class="pull-right" style="font-size: 30px;">0 元</h3>
		</div>
		<div class="tile-footer"><a href="#">檢視明細...<?php //echo $text_view; ?></a></div>
	</div>
</div>