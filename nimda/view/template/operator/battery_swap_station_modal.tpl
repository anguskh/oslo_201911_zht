<form method="post" action="<?=$modal_action?>" enctype="multipart/form-data" id="form-import-modal">
<div class="modal fade" id="modalPage" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				<h3 class="modal-title">电池租借站管理 编辑</h3>
			</div>
			<div class="modal-body">
				<div class="row">
					<input type="hidden" name="s_num">
					<div class="col-sm-6">
						<div class="form-group">
							<label id="l_location" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">交換站名稱</label>
							<input type="text" class="form-control" name="location" >
						</div>
						<div class="form-group">
							<label id="l_so_num" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">營運商名稱</label>
							<input type="text" class="form-control" name="operator_name" >
							<input type="hidden" class="form-control" name="so_num" >
						</div>
						<div class="form-group">
							<label id="l_status" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">交換站狀態</label>
							<select name="status" class="form-control">
								<OPTION value="">--請選擇--</OPTION>
								<OPTION value="Y">启用</OPTION>
								<OPTION value="N">停用</OPTION>
								<OPTION value="D">刪除</OPTION>
							</select>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label id="l_province" class="control-label">省份</label>
							<input type="text" class="form-control" name="province" >
						</div>
						<div class="form-group">
							<label id="l_city" class="control-label">城市</label>
							<input type="text" class="form-control" name="city" >
						</div>
						<div class="form-group">
							<label id="l_district" class="control-label">區域</label>
							<input type="text" class="form-control" name="district" >
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group">
							<label id="l_note" class="control-label">備註</label>
							<textarea class="form-control" rows="3" name="note"></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="bt_modal_save">存档</button>
				<button type="button" class="btn btn-success" data-dismiss="modal">取消</button>
			</div>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
$('.date').datetimepicker({
	pickTime: false,
});
$('input[name=\'operator_name\']').autocomplete({
	'source': function(request, response) {
		var url = "?route=operator/battery_swap_station/autocompleteOperator&token=" + getURLVar('token') ;
		$.ajax({
			url: url + '&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				console.log( json);
				json.unshift({
					category_id: 0,
					name: ' --- 無 --- '
				});

				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['so_num']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'operator_name\']').val(item['label']);
		$('input[name=\'so_num\']').val(item['value']);
	}
});

// 送出前檢查必填欄位
$('#bt_modal_save').click( function(){
	var filter = ['so_num', 'location', 'status'];
	var submitFlag = true ;
	for(var i=0; i<filter.length; i++) {
		// ---- Radio Buttons 的做法 ----
		if ( filter[i] == "top02") {
			var radioFlag = false ;
			$('input[name="'+filter[i]+'"]').each(function(){
				console.log( filter[i], $(this).val(), $(this).is(':checked'));
				if ( $(this).is(':checked')) {
					radioFlag = true ;
				}
			});
			if ( !radioFlag) {
				$('#l_'+filter[i]).tooltip('show') ;
				submitFlag = false ;
			} else {
				$('#l_'+filter[i]).tooltip('hide') ;
			}
		// ---- select 的做法 ----
		} else if ( filter[i] == "status") {
			console.log( "status : " + $('[name="'+filter[i]+'"]').val());
			if ( !$('[name="'+filter[i]+'"]').val()) {
				$('#l_'+filter[i]).tooltip('show') ;
				submitFlag = false ;
			} else {
				$('#l_'+filter[i]).tooltip('hide') ;
			}
			console.log( filter[i] + 'select submitFlag : ' + submitFlag);
		} else {
			if ( !$('[name="'+filter[i]+'"]').val()) {
				$('#l_'+filter[i]).tooltip('show') ;
				submitFlag = false ;
			} else {
				$('#l_'+filter[i]).tooltip('hide') ;
			}
		}
	}
	if ( submitFlag) {
		console.log(' Go Submit');
		$('#form-import-modal').submit() ;
	}
});
</script>