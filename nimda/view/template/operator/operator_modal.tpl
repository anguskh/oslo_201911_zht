<form method="post" action="<?=$modal_action?>" enctype="multipart/form-data" id="form-import-modal">
<div class="modal fade" id="modalPage" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				<h3 class="modal-title">营运商管理 编辑</h3>
			</div>
			<div class="modal-body">
				<div class="row">
					<input type="hidden" name="s_num">
					<div class="col-sm-6">
						<div class="form-group">
							<label id="l_top13" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">营运地區</label>
							<select class="form-control" name="top13" onchange="chang_tas(this.value)">
								<option value="">請選擇</option>
								<?php foreach ($selCountry as $cid => $row) : ?>
									<option value="<?=$cid?>"><?=$row['iso_code_2']?> <?=$row['iso_desc']?></option>
								<?php endforeach ; ?>
							</select>
						</div>
						<div class="form-group">
							<label id="l_top13" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">主機營運商</label>
							<select class="form-control" name="tas_idx" id="tas_idx">
								<option value="">請選擇</option>
								<?php foreach ($selOperatorApServer as $idx => $row) : ?>
									<option cid="<?=$row['cid']?>" value="<?=$row['idx']?>"><?=$row['server_name']?> <?=$row['server_host']?></option>
								<?php endforeach ; ?>
							</select>
						</div>


						<div class="form-group">
							<label id="l_top01" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">营运商名称</label>
							<input type="text" class="form-control" name="top01" >
						</div>
						<div class="form-group">
							<label id="l_top09" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">营运商代码</label>
							<input type="text" class="form-control" name="top09" >
						</div>
						<div class="form-group">
							<label id="l_top02" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">营运商型别</label>
							<input class="form-check-input" type="radio" name="top02" value="1">
							<label class="form-check-label" for="top021">直营</label>
							<input class="form-check-input" type="radio" name="top02" value="2">
							<label class="form-check-label" for="top022">加盟</label>
						</div>
						<div class="form-group">
							<label id="l_status" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">营运商状态</label>
							<input class="form-check-input" type="radio" name="status" value="Y">
							<label class="form-check-label" for="status1">启用</label>
							<input class="form-check-input" type="radio" name="status" value="N">
							<label class="form-check-label" for="status2">停用</label>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label id="l_top03" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">营运商连络人姓名</label>
							<input type="text" class="form-control" name="top03" >
						</div>
						<div class="form-group">
							<label class="control-label">营运商连络电话</label>
							<input type="text" class="form-control" name="top04" >
						</div>
						<div class="form-group">
							<label class="control-label">营运商行动电话</label>
							<input type="text" class="form-control" name="top05" >
						</div>
						<div class="form-group">
							<label class="control-label">营运商地址</label>
							<input type="text" class="form-control" name="top06" >
						</div>
						<div class="form-group">
							<label class="control-label">Email</label>
							<input type="text" class="form-control" name="top07" >
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="bt_modal_save">存档</button>
				<button type="button" class="btn btn-success" data-dismiss="modal">取消</button>
			</div>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
	// 送出前檢查必填欄位
$('#bt_modal_save').click( function(){
	var filter = ['top01', 'top09', 'top02', 'top03', 'top13', 'status'];
	var submitFlag = true ;
	for(var i=0; i<filter.length; i++) {
		// ---- radio 的做法 ----
		if ( filter[i] == "top02" || filter[i] == "status") {
			var radioFlag = false ;
			$('input[name="'+filter[i]+'"]').each(function(){
				console.log( filter[i], $(this).val(), $(this).is(':checked'));
				if ( $(this).is(':checked')) {
					radioFlag = true ;
				}
			});
			if ( !radioFlag) {
				$('#l_'+filter[i]).tooltip('show') ;
				submitFlag = false ;
			} else {
				$('#l_'+filter[i]).tooltip('hide') ;
			}
		// ---- select 的做法 ----
		} else if ( filter[i] == "top13") {
			console.log( "top13 : " + $('[name="'+filter[i]+'"]').val());
			if ( !$('[name="'+filter[i]+'"]').val()) {
				$('#l_'+filter[i]).tooltip('show') ;
				submitFlag = false ;
			} else {
				$('#l_'+filter[i]).tooltip('hide') ;
			}
			console.log( filter[i] + 'select submitFlag : ' + submitFlag);
		} else {
			if ( !$('[name="'+filter[i]+'"]').val()) {
				$('#l_'+filter[i]).tooltip('show') ;
				submitFlag = false ;
			} else {
				$('#l_'+filter[i]).tooltip('hide') ;
			}
		}
	}

	if ( submitFlag) {
		console.log(' Go Submit');
		$('#form-import-modal').submit() ;
	}
});

//依地區更改營運商主機
function chang_tas(val){
	$("#tas_idx option").show();
	$("#tas_idx option:selected").removeAttr("selected");
	$("#tas_idx option").hide();
	$("#tas_idx option[cid='"+val+"']").show();
}
</script>