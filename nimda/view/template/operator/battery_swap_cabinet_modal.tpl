<form method="post" action="<?=$modal_action?>" enctype="multipart/form-data" id="form-import-modal">
<div class="modal fade" id="modalPage" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				<h3 class="modal-title">機櫃管理 编辑</h3>
			</div>
			<div class="modal-body">
				<div class="row">
					<input type="hidden" name="s_num">
					<div class="col-sm-6">
						<div class="form-group">
							<label id="l_so_num" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">營運商名稱</label>
							<input type="text" class="form-control" name="operator_name" >
							<input type="hidden" class="form-control" name="so_num" >
						</div>
						<div class="form-group">
							<label id="l_ss_num" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">交換站名称</label>
							<input type="text" class="form-control" name="station_name" >
							<input type="hidden" class="form-control" name="ss_num" >
						</div>
						
						<div class="form-group">
							<label id="l_bss_id" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">交換站序號</label>
							<input type="text" class="form-control" name="bss_id" >
						</div>
						<div class="form-group">
							<label id="l_track_quantity" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">軌道數</label>
							<select class="form-control" name="track_quantity">
								<OPTION value="12">12</OPTION>
								<OPTION value="24">24</OPTION>
								<OPTION value="36">36</OPTION>
							</select>
						</div>
						<div class="form-group">
							<label id="l_sim_no" class="control-label">SIM卡卡号</label>
							<input type="text" class="form-control" name="sim_no" >
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label id="l_bss_token" class="control-label" data-toggle="tooltip">交換站Token ID</label>
							<input type="text" class="form-control" name="bss_token" >
						</div>
						<div class="form-group">
							<label id="l_version_no" class="control-label" data-toggle="tooltip">版本号码</label>
							<input type="text" class="form-control" name="version_no" >
						</div>
						<div class="form-group">
							<label id="l_version_date" class="control-label" data-toggle="tooltip">版本日期</label>
							<input type="text" class="form-control date" name="version_date" >
						</div>

						<div class="form-group">
							<label id="l_ip_type" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">IP類別</label>
							<select class="form-control" name="ip_type">
								<OPTION value="">--請選擇--</OPTION>
								<OPTION value="F">固定IP</OPTION>
								<OPTION value="C">變動IP</OPTION>
							</select>
						</div>
						<div class="form-group">
							<label id="l_ip" class="control-label">IP</label>
							<input type="text" class="form-control" name="ip" >
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group">
							<label id="l_note" class="control-label">備註</label>
							<textarea class="form-control" rows="3" name="note"></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="bt_modal_save">存档</button>
				<button type="button" class="btn btn-success" data-dismiss="modal">取消</button>
			</div>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
$('.date').datetimepicker({
	pickTime: false,
	format: 'YYYY-MM-DD'
});

//交換站
$('input[name=\'station_name\']').autocomplete({
	'source': function(request, response) {
		var so_num = $('input[name=\'so_num\']').val();
		console.log(so_num);
		var url = "?route=operator/battery_swap_station/autocompleteStation&token=" + getURLVar('token') ;
		$.ajax({
			url: url + '&filter_name=' +  encodeURIComponent(request) + '&so_num=' +  so_num,
			dataType: 'json',
			success: function(json) {
				console.log( json);
				json.unshift({
					category_id: 0,
					name: ' --- 無 --- '
				});

				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['ss_num']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'station_name\']').val(item['label']);
		$('input[name=\'ss_num\']').val(item['value']);
	}
});

//營運商
$('input[name=\'operator_name\']').autocomplete({
	'source': function(request, response) {
		var url = "?route=operator/battery_swap_station/autocompleteOperator&token=" + getURLVar('token') ;
		$.ajax({
			url: url + '&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				console.log( json);
				json.unshift({
					category_id: 0,
					name: ' --- 無 --- '
				});

				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['so_num']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'operator_name\']').val(item['label']);
		$('input[name=\'so_num\']').val(item['value']);
	}
});


	// 送出前檢查必填欄位
$('#bt_modal_save').click( function(){
	var filter = ['so_num','ss_num','bss_id','track_quantity'];
	var submitFlag = true ;
	for(var i=0; i<filter.length; i++) {
		if ( filter[i] == "top02" || filter[i] == "status") {
			var radioFlag = false ;
			$('input[name="'+filter[i]+'"]').each(function(){
				console.log( filter[i], $(this).val(), $(this).is(':checked'));
				if ( $(this).is(':checked')) {
					radioFlag = true ;
				}
			});
			if ( !radioFlag) {
				$('#l_'+filter[i]).tooltip('show') ;
				submitFlag = false ;
			} else {
				$('#l_'+filter[i]).tooltip('hide') ;
			}
		} else {
			if ( !$('[name="'+filter[i]+'"]').val()) {
				$('#l_'+filter[i]).tooltip('show') ;
				submitFlag = false ;
			} else {
				$('#l_'+filter[i]).tooltip('hide') ;
			}
		}
	}

	if ( submitFlag) {
		console.log(' Go Submit');
		$('#form-import-modal').submit() ;
	}
});
</script>