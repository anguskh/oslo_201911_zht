<form method="post" action="<?=$modal_action?>" enctype="multipart/form-data" id="form-import-modal">
<div class="modal fade" id="modalPage" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				<h3 class="modal-title">电池管理 编辑</h3>
			</div>
			<div class="modal-body">
				<div class="row">
					<input type="hidden" name="s_num">
					<div class="col-sm-6">
						<div class="form-group">
							<label id="l_do_num" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">營運商名稱</label>
							<input type="text" class="form-control" name="operator_name" >
							<input type="hidden" class="form-control" name="do_num" >
						</div>
						<div class="form-group">
							<label id="l_battery_id" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">电池序号</label>
							<input type="text" class="form-control" name="battery_id" >
						</div>
						<div class="form-group">
							<label id="l_manufacture_date" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">出廠日期</label>
							<div class="input-group date date-non">
							<input type="text" name="manufacture_date" placeholder="出廠日期" data-date-format="YYYY-MM-DD" class="form-control" />
							<span class="input-group-btn">
								<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
							</span>
							</div>
						</div>
						<div class="form-group">
							<label id="l_sys_no" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">版本号码</label>
							<input class="form-check-input" type="radio" name="sys_no" value="1">
							<label class="form-check-label" for="sys_no1">1.0</label>
							<input class="form-check-input" type="radio" name="sys_no" value="2">
							<label class="form-check-label" for="sys_no2">2.0</label>
						</div>
						<div class="form-group">
							<label id="l_position" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">电池位置</label>
							<input class="form-check-input" type="radio" name="position" value="B">
							<label class="form-check-label" for="positionB">bss</label>
							<input class="form-check-input" type="radio" name="position" value="V">
							<label class="form-check-label" for="positionV">電動機車</label>
							<input class="form-check-input" type="radio" name="position" value="O">
							<label class="form-check-label" for="positionO">其他</label>
						</div>
						<div class="form-group">
							<label id="l_status" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">電池狀態</label>
							<select name="status" class="form-control">
								<option value="1">已满电</option>
								<option value="2">故障</option>
								<option value="3">维修</option>
								<option value="4">报废</option>
								<option value="5">电池失窃</option>
								<option value="6">仓库</option>
							</select>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label id="l_battery_capacity" class="control-label" data-toggle="tooltip">SOC</label>
							<input type="text" class="form-control" name="battery_capacity" >
						</div>
						<div class="form-group">
							<label id="l_battery_health" class="control-label" data-toggle="tooltip">SOH</label>
							<input type="text" class="form-control" name="battery_health" >
						</div>
						<div class="form-group">
							<label id="l_battery_station_position" class="control-label" data-toggle="tooltip">基站位置(MCC，MNC，LAC，CELLID)</label>
							<input type="text" class="form-control" name="battery_station_position" >
						</div>
						<div class="form-group">
							<label id="l_battery_gps_manufacturer" class="control-label" data-toggle="tooltip" title="">GPS 制造商</label>
							<input type="text" class="form-control" name="battery_gps_manufacturer" >
						</div>
						<div class="form-group">
							<label id="l_battery_gps_version" class="control-label" data-toggle="tooltip" title="">GPS 固件版本</label>
							<input type="text" class="form-control" name="battery_gps_version" >
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="bt_modal_save">存档</button>
				<button type="button" class="btn btn-success" data-dismiss="modal">取消</button>
			</div>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
$('.date').datetimepicker({
	pickTime: false,
});
	// 送出前檢查必填欄位
$('#bt_modal_save').click( function(){
	var filter = ['do_num','battery_id','manufacture_date','position','sys_no'];
	var submitFlag = true ;
	for(var i=0; i<filter.length; i++) {
		if ( filter[i] == "position" || filter[i] == "sys_no") {
			var radioFlag = false ;
			$('input[name="'+filter[i]+'"]').each(function(){
				console.log( filter[i], $(this).val(), $(this).is(':checked'));
				if ( $(this).is(':checked')) {
					radioFlag = true ;
				}
			});
			if ( !radioFlag) {
				$('#l_'+filter[i]).tooltip('show') ;
				submitFlag = false ;ap_server_modal.tpl
			} else {
				$('#l_'+filter[i]).tooltip('hide') ;
			}
		} else {
			if ( !$('[name="'+filter[i]+'"]').val()) {
				$('#l_'+filter[i]).tooltip('show') ;
				submitFlag = false ;
			} else {
				$('#l_'+filter[i]).tooltip('hide') ;
			}
		}
	}

	if ( submitFlag) {
		console.log(' Go Submit');
		$('#form-import-modal').submit() ;
	}
});

//營運商
$('input[name=\'operator_name\']').autocomplete({
	'source': function(request, response) {
		var url = "?route=operator/battery_swap_station/autocompleteOperator&token=" + getURLVar('token') ;
		$.ajax({
			url: url + '&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				console.log( json);
				json.unshift({
					category_id: 0,
					name: ' --- 無 --- '
				});

				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['so_num']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'operator_name\']').val(item['label']);
		$('input[name=\'do_num\']').val(item['value']);
	}
});


</script>