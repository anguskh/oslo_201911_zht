<form method="post" action="<?=$modal_action?>" enctype="multipart/form-data" id="form-import-modal">
<div class="modal fade" id="modalPage" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				<h3 class="modal-title">營運主機管理 编辑</h3>
			</div>
			<div class="modal-body">
				<div class="row">
					<input type="hidden" name="idx">
					<div class="col-sm-6">
						<div class="form-group">
							<label id="l_cid" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">营运地區</label>
							<select class="form-control" name="cid">
								<option value="">請選擇</option>
								<?php foreach ($selCountry as $cid => $row) : ?>
									<option value="<?=$cid?>"><?=$row['iso_code_2']?> <?=$row['iso_desc']?></option>
								<?php endforeach ; ?>
							</select>
						</div>
						<div class="form-group">
							<label id="l_server_name" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">主機名称</label>
							<input type="text" class="form-control" name="server_name" >
						</div>
						<div class="form-group">
							<label id="l_server_host" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">主機營運商</label>
							<input type="text" class="form-control" name="server_host" >
						</div>
						<div class="form-group">
							<label id="l_server_location" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">主機位置</label>
							<input type="text" class="form-control" name="server_location" >
						</div>
						<div class="form-group">
							<label id="l_host_url" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">營運商網址</label>
							<input type="text" class="form-control" name="host_url" >
						</div>
						<div class="form-group">
							<label id="l_login_acc" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">登入帳號</label>
							<input type="text" class="form-control" name="login_acc" >
						</div>
						<div class="form-group">
							<label id="l_login_pwd" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">登入密碼</label>
							<input type="text" class="form-control" name="login_pwd" >
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label id="l_root_acc" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">主機帳號</label>
							<input type="text" class="form-control" name="root_acc" >
						</div>
						<div class="form-group">
							<label id="l_root_pwd" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">主機密碼</label>
							<input type="text" class="form-control" name="root_pwd" >
						</div>
						<div class="form-group">
							<label id="l_db_acc" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">資料庫帳號</label>
							<input type="text" class="form-control" name="db_acc" >
						</div>
						<div class="form-group">
							<label id="l_db_pwd" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">資料庫密碼</label>
							<input type="text" class="form-control" name="db_pwd" >
						</div>
						<div class="form-group">
							<label id="l_database_name" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">資料庫名稱</label>
							<input type="text" class="form-control" name="database_name" >
						</div>
						<div class="form-group">
							<label id="l_expire_date" class="control-label text-danger" data-toggle="tooltip" title="*此栏位不可空白">主機到期日</label>
							<div class="input-group date date-non">
							<input type="text" name="expire_date" value="2019-11-30" placeholder="主機到期日" data-date-format="YYYY-MM-DD" class="form-control" />
							<span class="input-group-btn">
								<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
							</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="bt_modal_save">存档</button>
				<button type="button" class="btn btn-success" data-dismiss="modal">取消</button>
			</div>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
$('.date').datetimepicker({
	pickTime: false,
});
	// 送出前檢查必填欄位
$('#bt_modal_save').click( function(){
	var filter = ['server_name','server_host','server_location','host_url','login_acc','login_pwd','root_acc','root_pwd','db_acc','db_pwd','database_name','expire_date'];
	var submitFlag = true ;
	for(var i=0; i<filter.length; i++) {
		if ( filter[i] == "top02" || filter[i] == "status") {
			var radioFlag = false ;
			$('input[name="'+filter[i]+'"]').each(function(){
				console.log( filter[i], $(this).val(), $(this).is(':checked'));
				if ( $(this).is(':checked')) {
					radioFlag = true ;
				}
			});
			if ( !radioFlag) {
				$('#l_'+filter[i]).tooltip('show') ;
				submitFlag = false ;ap_server_modal.tpl
			} else {
				$('#l_'+filter[i]).tooltip('hide') ;
			}
		} else {
			if ( !$('[name="'+filter[i]+'"]').val()) {
				$('#l_'+filter[i]).tooltip('show') ;
				submitFlag = false ;
			} else {
				$('#l_'+filter[i]).tooltip('hide') ;
			}
		}
	}

	if ( submitFlag) {
		console.log(' Go Submit');
		$('#form-import-modal').submit() ;
	}
});
</script>