<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a href="javascript:void(0);" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary" onclick="checkSubmit();"><i class="fa fa-upload"></i></a>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-upload" class="form-horizontal">
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-album"><?php echo $entry_download; ?></label>
                        <div class="col-sm-10">
                            <a href="<?php echo $download;?>" class="btn btn-danger"><i class="fa fa-download"></i><?php echo $button_download; ?></a>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-category"><?php echo $entry_upload_item; ?></label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <input type="text" name="item_filename" value="" class="form-control"  style="cursor: pointer;" onkeydown="return false;" onclick="file_upload('item');" />
                                <input type="file" name="item_file" value="" style="display:none;" onchange="checkValue(this,'item');">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-primary" onclick="file_upload('item');"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                </span> 
                            </div>
                            <?php if ($error_item) { ?>
                            <div class="text-danger"><?php echo $error_item; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-category"><?php echo $entry_upload_image; ?></label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <input type="text" name="image_filename" value="" class="form-control"  style="cursor: pointer;" onkeydown="return false;" onclick="file_upload('image');" />
                                <input type="file" name="image_file" value="" style="display:none;" onchange="checkValue(this,'image');">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-primary" onclick="file_upload('image');"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                </span> 
                            </div>
                            <?php if ($error_zip) { ?>
                            <div class="text-danger"><?php echo $error_zip; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php if(count($error_content)){?>
                    <div class="form-group required">
                        <?php foreach($error_content as $v) { ?>
                        <div class="text-danger text-center"><?php echo $v; ?></div>
                        <?php } ?>
                    </div>
                    <?php }?>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    function file_upload(type){
        $('[name="'+type+'_file"]').trigger('click');
    }
    function checkValue(obj,type){
        if(type=='item'){
            if(!/\.(csv|CSV)$/i.test($(obj).val())){
                $(obj).val('');
                alert('格式錯誤，請上傳CSV檔');
                return false;
            }
            else{
                $('[name="item_filename"]').val($(obj).val());
            }
        }
        else{
            if(!/\.(zip|ZIP)$/i.test($(obj).val())){
                $(obj).val('');
                alert('格式錯誤，請上傳ZIP檔');
                return false;
            }
            else{
                $('[name="image_filename"]').val($(obj).val());
            }
        }
    }
    
    function checkSubmit(){
        var err = new Array();
        if($('[name="item_file"]').val()==""){
            err[err.length] = "請上傳相片CSV檔";
        }
        if($('[name="image_file"]').val()==""){
            err[err.length] = "請上傳相片ZIP檔";
        }
        
        if(err.length>0){
            alert(err.join('\n'));
            return false;
        }
        else{
            $('#form-upload').submit();
        }
    }
    </script>
</div>
<?php echo $footer; ?>
