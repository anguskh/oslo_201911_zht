<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php foreach ($rows as $iCnt => $row) : ?>
		<div class="row">
			<?php if (count( $row) > 1) : ?>
				<?php foreach ($row as $iCnt => $block) : ?>
					<?php echo $block['output'] ; ?>
				<?php endforeach ; ?>
			<?php else : ?>
				<?php echo $row[0]['output'] ; ?>
			<?php endif ; ?>
		</div>
		<?php endforeach ; ?>
	</div>
</div>
<?php echo $footer; ?>
