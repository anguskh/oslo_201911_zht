
/**
 * [modal page 關閉時會呼叫的動作]
 * @Another Angus
 * @date    2019-11-29
 */
$("#modalPage").on("hide.bs.modal",function(e){
	console.log('關閉視窗前呼叫');
	$('.text-danger').tooltip('hide') ;
});


/**
 * [openModal description]
 * @param   {[type]}   id [description]
 * @return  {[type]}      [description]
 * @Another Angus
 * @date    2019-11-28
 */
function openModal( id, act) {
	console.log( "modalPage Operator") ;
	console.log( id);
	console.log('act : ' + act);
	var url    = "?route=operator/battery/ajaxEditRowData&token=" + getURLVar('token') ;
	var filter = ['s_num', 'do_num', 'operator_name', 'battery_id', 'manufacture_date', 'position', 'sys_no', 'battery_capacity', 'battery_health','battery_station_position','battery_gps_manufacturer','battery_gps_version','status'];
	switch( act) {
		case 'a':
			$('.modal-title').html('电池管理 新增') ;
				for(var i=0;i<filter.length;i++) {
					// ---- Radio Buttons 的做法 ----
					if ( filter[i] == "top02") {
						$('input[name="'+filter[i]+'"]').each(function(){
							$(this).prop('checked','');
						});
					// ---- select 的做法 ----
					} else if ( filter[i] == "status") {
						$('select[name="'+filter[i]+'"]').val('') ;
					} else {
						$('[name="'+filter[i]+'"]').val( '');
					}
				}
			break;
		case 'e':
			$('.modal-title').html('电池管理 编辑') ;
			$.ajax({
				url: url +'&idx='+ id,
				dataType: 'json',
				success: function(resp) {
					for(var i=0;i<filter.length;i++) {
						// console.log( filter[i], resp[filter[i]]);
						// ---- Radio Buttons 的做法 ----
						if ( filter[i] == "position" ||  filter[i] == "sys_no") {
							$('input[name="'+filter[i]+'"]').each(function(){
								$(this).prop('checked','');
							});
							$('input[name="'+filter[i]+'"][value="'+resp[filter[i]]+'"]').prop('checked','checked');
						// ---- select 的做法 ----
						} else if ( filter[i] == "status") {
							// console.log( filter[i], resp[filter[i]]);
							$('select[name="'+filter[i]+'"]').val( resp[filter[i]]) ;
						} else {
							$('[name="'+filter[i]+'"]').val( resp[filter[i]]);
						}
					}
				}
			});
			break;
	}
	$("#modalPage").modal('show') ;
}
