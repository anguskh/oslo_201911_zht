
/**
 * [modal page 關閉時會呼叫的動作]
 * @Another Angus
 * @date    2019-11-29
 */
$("#modalPage").on("hide.bs.modal",function(e){
	console.log('關閉視窗前呼叫');
	$('.text-danger').tooltip('hide') ;
});


/**
 * [openModal description]
 * @param   {[type]}   id [description]
 * @return  {[type]}      [description]
 * @Another Angus
 * @date    2019-11-28
 */
function openModal( id, act) {
	console.log( "modalPage ap_server") ;
	console.log( id);
	console.log( act);
	var url    = "?route=operator/ap_server/ajaxEditRowData&token=" + getURLVar('token') ;
	var filter = ['idx','cid','server_name','server_host','server_location','host_url','login_acc','login_pwd','root_acc','root_pwd','db_acc','db_pwd','database_name','expire_date'];

	switch( act) {
		case 'a':
			$('.modal-title').html('營運主機管理 新增') ;
				for(var i=0;i<filter.length;i++){
					// radio 的做法
					if ( filter[i] == "top02" || filter[i] == "status") {
						$('input[name="'+filter[i]+'"]').each(function(){
							$(this).prop('checked','');
						});
					// ---- select 的做法 ----
					} else if ( filter[i] == "cid") {
						$('select[name="'+filter[i]+'"]').val('') ;
					} else {
						$('[name="'+filter[i]+'"]').val( '');
					}
				}
			break;
		case 'e':
			$('.modal-title').html('營運主機管理 编辑') ;
			$.ajax({
				url: url +'&idx='+ id,
				dataType: 'json',
				success: function(resp) {
					console.log( resp);
					for(var i=0;i<filter.length;i++) {
						console.log( filter[i], resp[filter[i]]);
						// radio 的做法
						if ( filter[i] == "top02" || filter[i] == "status") {
							$('input[name="'+filter[i]+'"]').each(function(){
								$(this).prop('checked','');
							});
							$('input[name="'+filter[i]+'"][value="'+resp[filter[i]]+'"]').prop('checked','checked');
						// ---- select 的做法 ----
						} else if ( filter[i] == "cid") {
							// console.log( filter[i], resp[filter[i]]);
							$('select[name="'+filter[i]+'"]').val( resp[filter[i]]) ;
						} else {
							$('[name="'+filter[i]+'"]').val( resp[filter[i]]);
						}
					}
				}
			});
			break;
	}
	$("#modalPage").modal('show') ;
}
