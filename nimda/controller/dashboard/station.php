<?php
/**
 * 交換站數量
 * @Another Angus
 * @date    2020-02-05
 */
class ControllerDashboardStation extends Controller {
	/**
	 * [dashboard description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2020-01-28
	 */
	public function dashboard() {
		// $this->load->language('extension/dashboard/activity') ;

		// $data['heading_title'] = $this->language->get('heading_title') ;

		// $data['text_no_results'] = $this->language->get('text_no_results') ;

		$data['token'] = $this->session->data['token'] ;

		$this->load->model('operator/battery_swap_station') ;
		$data['cnt'] = $this->model_operator_battery_swap_station->dashboardCnt() ;

		return $this->load->view('dashboard/station', $data);
	}
}
