<?php
/**
 * 所有狀態 --> 目前寫死view
 * @Another Angus
 * @date    2020-02-05
 */
class ControllerDashboardAllstatus extends Controller {
	/**
	 * [dashboard description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2020-02-05
	 */
	public function dashboard() {
		// $this->load->language('extension/dashboard/activity') ;

		// $data['heading_title'] = $this->language->get('heading_title') ;

		// $data['text_no_results'] = $this->language->get('text_no_results') ;

		$data['token'] = $this->session->data['token'] ;

		$this->load->model('operator/battery') ;
		$data['cnt'] = $this->model_operator_battery->dashboardCnt() ;

		return $this->load->view('dashboard/allstatus', $data);
	}
}
