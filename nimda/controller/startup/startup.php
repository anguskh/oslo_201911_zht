<?php
class ControllerStartupStartup extends Controller {
	public function index() {
		if ( DB_DEBUG_MOD) {
			$runUrl   = isset( $_REQUEST["route"]) ? $_REQUEST["route"] : "" ;
			$userName = isset( $this->session->data['user_name']) ? $this->session->data['user_name'] : "" ;
			$logFileName = DIR_LOGS."db.log" ;
			$fp = fopen($logFileName , "a" ) ;
			$nowTime = date("Y/m/d H:i:s");
			fwrite($fp,"{$nowTime} | --- ROUTE : {$runUrl}  --- USER : {$userName}\r\n") ;
			fwrite($fp,"{$nowTime} | URL ROUTE : {$_SERVER["REQUEST_URI"]} \r\n") ;
			fclose( $fp) ;
		}
	}
}