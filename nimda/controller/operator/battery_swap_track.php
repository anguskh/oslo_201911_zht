<?php
/**
 * [ControllerOperatorBatterySwapTrack 电池租借轨道管理]
 * @Another Angus
 * @date    2019-11-27
 */
class ControllerOperatorBatterySwapTrack extends Controller {
	private $error      = array();
	private $column_url = '' ;
	private $page_url   = '';
	private $func_path  = 'operator/battery_swap_track' ;

	/**
	 * [index description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-11-21
	 */
	public function index() {
		$this->load->language( $this->func_path);

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model( $this->func_path);

		$this->getList();
	}

	/**
	 * [getList description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-11-21
	 */
	protected function getList() {
		$data = $this->init() ;
		// dump( $this->column_url) ;
		// dump( $this->page_url) ;
		// get參數==========================================================================================================
		$data["page"] = !empty($data["page"]) ? $data["page"] : 1;
		$data["filter_limit"] = !empty($data["filter_limit"]) ? $data["filter_limit"] : $this->config->get('config_limit_admin');

		// 取得排序資訊
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'default';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		$url = $this->column_url;
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		// 欄位排序條件
		$data['colSort']['so_num']        = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=so_num' . $url, true);
		$data['colSort']['sb_num']        = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=sb_num' . $url, true);
		$data['colSort']['track_no']      = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=track_no' . $url, true);
		$data['colSort']['column_park']   = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=column_park' . $url, true);
		$data['colSort']['column_charge'] = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=column_charge' . $url, true);
		$data['colSort']['battery_id']    = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=battery_id' . $url, true);
		$data['colSort']['status']        = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=status' . $url, true);

		// 設定列表欄位 ---------------------------------------------------------------------------------
		$columnNames = array(
				"so_num"        => $this->language->get('column_so_num'),
				"sb_num"        => $this->language->get('column_sb_num'),
				"track_no"      => $this->language->get('column_track_no'),
				"column_park"   => $this->language->get('column_column_park'),
				"column_charge" => $this->language->get('column_column_charge'),
				"battery_id"    => $this->language->get('column_battery_id'),
				"history"       => $this->language->get('column_history'),
				"status"        => $this->language->get('column_status'),
			) ;
		$data['columnNames'] = $columnNames ;
		$data['td_colspan']  = count( $columnNames) + 2 ;


		$data['heading_title']   = $this->language->get('heading_title');

		$data['text_list']       = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm']    = $this->language->get('text_confirm');

		$data['column_action']   = $this->language->get('column_action');

		// 查詢條件 ------------------------------------------------------------------------------------
		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,

			'start' => ($data["page"] - 1) * $data["filter_limit"],
			'limit' => $data["filter_limit"]
		);
		$total            = $this->model_operator_battery_swap_track->getTotalConfigs();
		$results          = $this->model_operator_battery_swap_track->getConfigs( $filter_data) ;
		$data['listRows'] = $results ;

		$url = $this->page_url;
		$pagination         = new Pagination();
		$pagination->total  = $total;
		$pagination->page   = $data["page"];
		$pagination->limit  = $data["filter_limit"];
		$pagination->url    = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$data['pagination'] = $pagination->render();
		$data['results']    = sprintf($this->language->get('text_pagination'), ($total) ? (($data["page"] - 1) * $data["filter_limit"]) + 1 : 0, ((($data["page"] - 1) * $data["filter_limit"]) > ($total - $data["filter_limit"])) ? $total : ((($data["page"] - 1) * $data["filter_limit"]) + $data["filter_limit"]), $total, ceil($total / $data["filter_limit"]));

		$data['sort']  = $sort;
		$data['order'] = $order;

		// 程式最後====================================================================================
		// ==========================================================================================
		// ==========================================================================================
		// ==========================================================================================
		$this->response->setOutput($this->load->view('operator/battery_swap_track_list', $data));
	}

	/**
	 * [init description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-11-22
	 */
	protected function init() {
		$this->load->language( $this->func_path);
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model( $this->func_path);

		// message area============================================================================
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		// 麵包屑 Start ------------------------------------------------------------------------------
		$data['breadcrumbs'] = array();
		$url = "" ;
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . $url, true)
		);
		// 麵包屑 End ---------------------------------------------------------------------------------

		// 整理列表搜尋條件
		$filterArr = array (


			"sort"         => "排序升降冪條件" ,
			"order"        => "欄位排序" ,
			"page"         => "頁碼" ,
			// "filter_limit" => "分頁數" ,
		) ;
		foreach ($filterArr as $col => $name) {
			$data["{$col}"] = isset($this->request->get["{$col}"]) ? trim($this->request->get["{$col}"]) : "";

			if (isset($this->request->get["{$col}"]) && !empty($this->request->get["{$col}"])) {
				// 欄位
				if ( $col != "order" && $col != "sort") {
					$this->column_url .= "&{$col}=" . $this->request->get["{$col}"] ;
				}
				// 分頁
				if ( $col != "page") {
					$this->page_url .= "&{$col}=" . $this->request->get["{$col}"] ;
				}
			}
		}


		// 程式最後=============================================================
		$data['header']      = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer']      = $this->load->controller('common/footer');


		return $data ;
	}

}