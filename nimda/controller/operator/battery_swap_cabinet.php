<?php
/**
 * [ControllerOperatorBatterySwapCabinet 機櫃管理]
 * @Another Angus
 * @date    2019-12-01
 */
class ControllerOperatorBatterySwapCabinet extends Controller {
	private $error      = array();
	private $column_url = '' ;
	private $page_url   = '';
	private $func_path  = 'operator/battery_swap_cabinet' ;

	/**
	 * [index description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-11-21
	 */
	public function index() {
		$this->load->language( $this->func_path);

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model( $this->func_path);

		$this->getList();
	}

	/**
	 * [getList description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-11-21
	 */
	protected function getList() {
		$data = $this->init() ;
		$data['t'] = time() ;
		// dump( $this->column_url) ;
		// dump( $this->page_url) ;
		// get參數==========================================================================================================
		$data["page"] = !empty($data["page"]) ? $data["page"] : 1;
		$data["filter_limit"] = !empty($data["filter_limit"]) ? $data["filter_limit"] : $this->config->get('config_limit_admin');

		// 取得排序資訊
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'default';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		$url = $this->column_url;
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		// 欄位排序條件
		$data['colSort']['so_num']      = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=so_num' . $url, true);
		$data['colSort']['ss_num']      = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=ss_num' . $url, true);
		$data['colSort']['bss_id']       = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=bss_id' . $url, true);
		$data['colSort']['ip_type']      = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=ip_type' . $url, true);
		$data['colSort']['ip']           = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=ip' . $url, true);
		$data['colSort']['last_online']  = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=last_online' . $url, true);
		$data['colSort']['version_no']   = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=version_no' . $url, true);
		$data['colSort']['version_date'] = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=version_date' . $url, true);
		$data['colSort']['status'] = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=status' . $url, true);

		// 設定列表欄位 ---------------------------------------------------------------------------------
		$columnNames = array(
				"so_num"       => $this->language->get('column_so_num'),
				"ss_num"       => $this->language->get('column_ss_num'),
				"bss_id"       => $this->language->get('column_bss_id'),
				"track_quantity"   => $this->language->get('column_track_quantity'),
				"ip_type"      => $this->language->get('column_ip_type'),
				"ip"           => $this->language->get('column_ip'),
				"last_online"  => $this->language->get('column_last_online'),
				"version_no"   => $this->language->get('column_version_no'),
				"version_date" => $this->language->get('column_version_date'),
				"status"       => $this->language->get('column_status'),
			) ;
		$data['column_action'] = $this->language->get('column_action');
		$data['columnNames']   = $columnNames ;
		$data['td_colspan']    = count( $columnNames) + 2 ;


		$data['heading_title']   = $this->language->get('heading_title');

		$data['text_list']       = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm']    = $this->language->get('text_confirm');

		$data['button_add']      = $this->language->get('text_add');
		$data['button_edit']     = $this->language->get('text_edit');
		$data['button_delete']   = $this->language->get('text_delete');

		// modal page 彈跳視窗
		$data['modal_action'] = $this->url->link($this->func_path . '/edit', 'token=' . $this->session->data['token'] . $url, true);
		$data['del_action']   = $this->url->link($this->func_path . '/del', 'token=' . $this->session->data['token'] . $url, true);

		// 查詢條件 ------------------------------------------------------------------------------------
		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,

			'start' => ($data["page"] - 1) * $data["filter_limit"],
			'limit' => $data["filter_limit"]
		);

		$this->load->model( 'operator/operator');
		$this->load->model( 'operator/battery_swap_station');

		$ipStatus  = $this->config->get( 'ip_status') ;
		$strStatus = $this->config->get( 'status') ;
		$operatorNameArr = $this->model_operator_operator->getOperator() ;
		$stationNameArr = $this->model_operator_battery_swap_station->getStation() ;
		$total     = $this->model_operator_battery_swap_cabinet->getTotalBSC();
		$results   = $this->model_operator_battery_swap_cabinet->getBSC( $filter_data) ;
		foreach ($results as $iCnt => $tmpRow) {
			$results[$iCnt]['so_num'] = isset($operatorNameArr[$tmpRow['so_num']]) ? $operatorNameArr[$tmpRow['so_num']] : "" ;
			$results[$iCnt]['ss_num'] = isset($stationNameArr[$tmpRow['ss_num']]) ? $stationNameArr[$tmpRow['ss_num']] : "" ;
			$results[$iCnt]['ip_type'] = isset( $ipStatus[$tmpRow['ip_type']]) ? $ipStatus[$tmpRow['ip_type']] : "" ;
			$results[$iCnt]['status']  = isset( $strStatus[$tmpRow['status']]) ? $strStatus[$tmpRow['status']] : "" ;
		}

		$data['listRows'] = $results ;

		$url = $this->page_url;
		$pagination         = new Pagination();
		$pagination->total  = $total;
		$pagination->page   = $data["page"];
		$pagination->limit  = $data["filter_limit"];
		$pagination->url    = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$data['pagination'] = $pagination->render();
		$data['results']    = sprintf($this->language->get('text_pagination'), ($total) ? (($data["page"] - 1) * $data["filter_limit"]) + 1 : 0, ((($data["page"] - 1) * $data["filter_limit"]) > ($total - $data["filter_limit"])) ? $total : ((($data["page"] - 1) * $data["filter_limit"]) + $data["filter_limit"]), $total, ceil($total / $data["filter_limit"]));

		$data['sort']  = $sort;
		$data['order'] = $order;

		// 程式最後====================================================================================
		// ==========================================================================================
		// ==========================================================================================
		// ==========================================================================================
		$this->response->setOutput($this->load->view('operator/battery_swap_cabinet_list', $data));
	}

	/**
	 * [init description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-11-22
	 */
	protected function init() {
		$this->load->language( $this->func_path);
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model( $this->func_path);

		// message area============================================================================
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		// 麵包屑 Start ------------------------------------------------------------------------------
		$data['breadcrumbs'] = array();
		$url = "" ;
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . $url, true)
		);
		// 麵包屑 End ---------------------------------------------------------------------------------

		// 整理列表搜尋條件
		$filterArr = array (
			"sort"         => "排序升降冪條件" ,
			"order"        => "欄位排序" ,
			"page"         => "頁碼" ,
			// "filter_limit" => "分頁數" ,
		) ;
		foreach ($filterArr as $col => $name) {
			$data["{$col}"] = isset($this->request->get["{$col}"]) ? trim($this->request->get["{$col}"]) : "";

			if (isset($this->request->get["{$col}"]) && !empty($this->request->get["{$col}"])) {
				// 欄位
				if ( $col != "order" && $col != "sort") {
					$this->column_url .= "&{$col}=" . $this->request->get["{$col}"] ;
				}
				// 分頁
				if ( $col != "page") {
					$this->page_url .= "&{$col}=" . $this->request->get["{$col}"] ;
				}
			}
		}


		// 程式最後=============================================================
		$data['header']      = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer']      = $this->load->controller('common/footer');


		return $data ;
	}
	/**
	 * [edit description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-12-12
	 */
	public function edit() {
		$this->load->model( $this->func_path) ;
		$this->load->language( $this->func_path) ;

		// dump( $this->request->post) ;
		// dump( $this->request->get) ;

		if ( $this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateForm()) {
			$insInfo = $this->request->post ;

			$s_num = trim( $this->request->post['s_num']);
			if ( empty( $s_num )) {
				$this->model_operator_battery_swap_cabinet->addResource( $insInfo) ;
			} else {
				$this->model_operator_battery_swap_cabinet->editResource( $insInfo) ;
			}
			$this->session->data['success'] = $this->language->get('text_success');

			// 排除名單
			$allowed = array( "token", "route") ;
			// 組合URL
			$urlArr = array() ;
			foreach ($this->request->get as $keyName => $value) {
				if ( !in_array( $keyName, $allowed)) {
					$urlArr[] = "{$keyName}={$value}" ;
				}
			}

			// exit() ;
			$url = "&".implode('&', $urlArr) ;
			$this->response->redirect($this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . $url, true));
		}
		$this->getList();

	}
	/**
	 * [ajaxOperator description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-12-11
	 */
	public function ajaxEditRowData() {
		$this->load->model( $this->func_path) ;
		$filter_data = array(
			"s_num" => $this->request->get['idx'],
		);
		$results = $this->model_operator_battery_swap_cabinet->ajaxGetBSC( $filter_data) ;
		// dump( $results[0]) ;
		$json = isset( $results[0]) ? $results[0] : array() ;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	/**
	 * [validateForm description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-12-12
	 */
	protected function validateForm () {
		// 檢查是否有權限
		if (!$this->user->hasPermission('modify', $this->func_path)) {
			$this->error['warning'] = $this->language->get('error_permission') ;
		} else {
			if ( !isset( $this->request->post['so_num']) && !trim( $this->request->post['so_num'])) {
				$this->error['warning'] = "營運商為必填欄位" ;
			}
			if ( !isset( $this->request->post['ss_num']) && !trim( $this->request->post['ss_num'])) {
				$this->error['warning'] = "交換站名称為必填欄位" ;
			}
			if ( !isset( $this->request->post['bss_id']) && !trim( $this->request->post['bss_id'])) {
				$this->error['warning'] = "交換站序號為必填欄位" ;
			}

			if(isset( $this->request->post['bss_id'])){
				//檢查交換站序號是否重覆
				$bss_flag = $this->model_operator_battery_swap_cabinet->countbss_id($this->request->post['bss_id'], $this->request->post['s_num']) ;
				if(!$bss_flag){
					$this->error['warning'] = "交換站序號重覆" ;
				}
			}
			if ($this->error && !isset($this->error['warning'])) {
				$this->error['warning'] = $this->language->get('error_warning');
			}
		}
		// dump( $this->error) ;
		return !$this->error;
	}

	public function del() {
		$this->load->model( $this->func_path) ;
		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $s_num) {
				$this->model_operator_battery_swap_cabinet->delResource($s_num);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('operator/battery_swap_cabinet', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'operator/operator')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

}