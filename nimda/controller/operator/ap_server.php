<?php
/**
 * [ControllerOperatorApServer 營運主機管理]
 * @Another Angus
 * @date    2019-11-30
 */
class ControllerOperatorApServer extends Controller {
	private $error      = array();
	private $column_url = '' ;
	private $page_url   = '';
	private $func_path  = 'operator/ap_server' ;

	/**
	 * [index description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-11-21
	 */
	public function index() {
		$this->load->language( $this->func_path);

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model( $this->func_path);

		$this->getList();
	}

	/**
	 * [edit description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-11-29
	 */
	public function edit() {
		$this->load->model( $this->func_path) ;
		$this->load->language( $this->func_path) ;

		// dump( $this->request->post) ;
		// dump( $this->request->get) ;
		if ( $this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateForm()) {
			// dump( $this->request->post) ;
			// exit() ;
			$insInfo = $this->request->post ;
			$idx = trim( $insInfo['idx']);
			if ( empty( $idx )) {
				$this->model_operator_ap_server->addResource( $insInfo) ;
			} else {
				$this->model_operator_ap_server->editResource( $insInfo) ;
			}
			$this->session->data['success'] = $this->language->get('text_success');
		}

		// 排除名單
		$allowed = array( "token", "route") ;
		// 組合URL
		$urlArr = array() ;
		foreach ($this->request->get as $keyName => $value) {
			if ( !in_array( $keyName, $allowed)) {
				$urlArr[] = "{$keyName}={$value}" ;
			}
		}

		$url = "&".implode('&', $urlArr) ;
		$this->response->redirect($this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . $url, true));
	}

	/**
	 * [ajaxOperator description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-11-28
	 */
	public function ajaxEditRowData() {
		$this->load->model( $this->func_path) ;
		$filter_data = array(
			"idx" => $this->request->get['idx'],
		);
		$results          = $this->model_operator_ap_server->getOperatorApServer( $filter_data) ;
		// dump( $results[0]) ;
		$json = isset( $results[0]) ? $results[0] : array() ;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	/**
	 * [getList description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-11-21
	 */
	protected function getList() {
		$data = $this->init() ;
		$data['t'] = time() ;
		// dump( $this->column_url) ;
		// dump( $this->page_url) ;
		// get參數==========================================================================================================
		$data["page"] = !empty($data["page"]) ? $data["page"] : 1;
		$data["filter_limit"] = !empty($data["filter_limit"]) ? $data["filter_limit"] : $this->config->get('config_limit_admin');

		// 取得排序資訊
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'default';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		$url = $this->column_url;
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		// 欄位排序條件
		$data['colSort']['cid'] = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=cid' . $url, true);
		$data['colSort']['server_name'] = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=server_name' . $url, true);
		$data['colSort']['server_host'] = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=server_host' . $url, true);
		$data['colSort']['server_location'] = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=server_location' . $url, true);
		$data['colSort']['expire_date'] = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=expire_date' . $url, true);
		$data['colSort']['status'] = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=status' . $url, true);

		// 設定列表欄位 ---------------------------------------------------------------------------------
		$columnNames = array(
				"cid"             => $this->language->get('column_country'),
				"server_name"     => $this->language->get('column_server_name'),
				"server_host"     => $this->language->get('column_server_host'),
				"server_location" => $this->language->get('column_server_location'),
				"host_url"        => $this->language->get('column_host_url'),
				"expire_date"     => $this->language->get('column_expire_date'),
				"status"          => $this->language->get('column_status'),
			) ;
		$data['column_action']   = $this->language->get('column_action');
		$data['columnNames']     = $columnNames ;
		$data['td_colspan']      = count( $columnNames) + 2 ;


		$data['heading_title']   = $this->language->get('heading_title');

		$data['text_list']       = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm']    = $this->language->get('text_confirm');

		$data['button_add']      = $this->language->get('text_add');
		$data['button_edit']     = $this->language->get('text_edit');
		$data['button_delete']   = $this->language->get('text_delete');

		// modal page 彈跳視窗
		$data['modal_action'] = $this->url->link($this->func_path . '/edit', 'token=' . $this->session->data['token'] . $url, true);
		$data['del_action']   = $this->url->link($this->func_path . '/del', 'token=' . $this->session->data['token'] . $url, true);

		// 查詢條件 ------------------------------------------------------------------------------------
		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,

			'start' => ($data["page"] - 1) * $data["filter_limit"],
			'limit' => $data["filter_limit"]
		);
		$this->load->model( 'operator/country') ;
		$statusArr = $this->config->get( 'operator_status') ;

		$countryInfo = $this->model_operator_country->getCountryList() ;
		$data['selCountry']  = $countryInfo ;

		$total            = $this->model_operator_ap_server->getTotalOperatorApServer();
		$results          = $this->model_operator_ap_server->getOperatorApServer( $filter_data) ;
		foreach ($results as $iCnt => $row) {
			$results[$iCnt]['status'] = $statusArr[$row['status']] ;
			$results[$iCnt]['cid']  = isset( $countryInfo[$row['cid']]) ? $countryInfo[$row['cid']]['iso_desc'] : "" ;
		}
		$data['listRows'] = $results ;

		$url = $this->page_url;
		$pagination         = new Pagination();
		$pagination->total  = $total;
		$pagination->page   = $data["page"];
		$pagination->limit  = $data["filter_limit"];
		$pagination->url    = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$data['pagination'] = $pagination->render();
		$data['results']    = sprintf($this->language->get('text_pagination'), ($total) ? (($data["page"] - 1) * $data["filter_limit"]) + 1 : 0, ((($data["page"] - 1) * $data["filter_limit"]) > ($total - $data["filter_limit"])) ? $total : ((($data["page"] - 1) * $data["filter_limit"]) + $data["filter_limit"]), $total, ceil($total / $data["filter_limit"]));

		$data['sort']  = $sort;
		$data['order'] = $order;

		// 程式最後====================================================================================
		// ==========================================================================================
		// ==========================================================================================
		// ==========================================================================================
		$this->response->setOutput($this->load->view('operator/ap_server_list', $data));
	}

	/**
	 * [init description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-11-22
	 */
	protected function init() {
		$this->load->language( $this->func_path);
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model( $this->func_path);

		// message area============================================================================
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		// 麵包屑 Start ------------------------------------------------------------------------------
		$data['breadcrumbs'] = array();
		$url = "" ;
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . $url, true)
		);
		// 麵包屑 End ---------------------------------------------------------------------------------

		// 整理列表搜尋條件
		$filterArr = array (


			"sort"         => "排序升降冪條件" ,
			"order"        => "欄位排序" ,
			"page"         => "頁碼" ,
			// "filter_limit" => "分頁數" ,
		) ;
		foreach ($filterArr as $col => $name) {
			$data["{$col}"] = isset($this->request->get["{$col}"]) ? trim($this->request->get["{$col}"]) : "";

			if (isset($this->request->get["{$col}"]) && !empty($this->request->get["{$col}"])) {
				// 欄位
				if ( $col != "order" && $col != "sort") {
					$this->column_url .= "&{$col}=" . $this->request->get["{$col}"] ;
				}
				// 分頁
				if ( $col != "page") {
					$this->page_url .= "&{$col}=" . $this->request->get["{$col}"] ;
				}
			}
		}


		// 程式最後=============================================================
		$data['header']      = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer']      = $this->load->controller('common/footer');


		return $data ;
	}

	/**
	 * [validateForm description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-11-29
	 */
	protected function validateForm () {
		// 檢查是否有權限
		if (!$this->user->hasPermission('modify', $this->func_path)) {
			$this->error['warning'] = $this->language->get('error_permission') ;
		} else {
			if ( !isset( $this->request->post['server_name']) && !trim( $this->request->post['server_name'])) {
				$this->error['server_name'] = "营运商名称為必填欄位" ;
			}


			if ($this->error && !isset($this->error['warning'])) {
				$this->error['warning'] = $this->language->get('error_warning');
			}
		}
		// dump( $this->error) ;
		return !$this->error;
	}

}