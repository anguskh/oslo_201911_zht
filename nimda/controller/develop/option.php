<?php
class ControllerDevelopOption extends Controller {
     private $error = array();

     protected $pathTemplate = DIR_INITIAL."/coding_template/" ;

	/**
     * 列表 頁面
     * @param  [type] $method [description]
     * @return [type]         [description]
     */
	public function index() {
          $this->load->language('develop/option');
          $this->document->setTitle($this->language->get('heading_title'));
          $this->getForm();
	}

	/**
     * 新增 / 修改 頁面
     * @param  [type] $method [description]
     * @return [type]         [description]
     */
	public function add() {
          $this->load->language('develop/option');
          $this->document->setTitle($this->language->get('heading_title'));

          if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
               // 檢查各目錄是否存在

               if ( empty($this->request->post['del_func'])) {
                    $classFolder = $this->request->post['new_func_path'] ;
                    $className = $this->request->post['new_func_class'] ;
               } else {
                    list( $classFolder, $className) = explode('/', $this->request->post['del_func']) ;
               }

               $cPath = DIR_APPLICATION."controller/".$classFolder ;
               $lPath = DIR_APPLICATION."language/zh-TW/".$classFolder ;
               $mPath = DIR_APPLICATION."model/".$classFolder ;
               $vPath = DIR_APPLICATION."view/template/".$classFolder ;

               if ( empty($this->request->post['del_func'])) {
                    // dump( "進行 -新增- 功能作業") ;
                    // controller
                    $this->setupFunction( 'controller',     $cPath, $className) ;
                    // languahe
                    $this->setupFunction( 'language',       $lPath, $className) ;
                    // model
                    $this->setupFunction( 'model',          $mPath, $className) ;
                    // view
                    $this->setupFunction( 'view',           $vPath, $className) ;
               } else { // 進行刪除功能作業
                    // dump( "進行 -刪除- 功能作業") ;
                    // controller
                    $this->removeFunction( 'controller',     $cPath, $className) ;
                    // languahe
                    $this->removeFunction( 'language',       $lPath, $className) ;
                    // model
                    $this->removeFunction( 'model',          $mPath, $className) ;
                    // view
                    $this->removeFunction( 'view',           $vPath, $className) ;
               }
          }


          $this->getForm();
	}

	/**
     * 新增 / 修改 頁面
     * @param  [type] $method [description]
     * @return [type]         [description]
     */
	protected function getForm() {
          // 前置作業 麵包屑,標題,按鈕
          $data = $this->preparation() ;

          $url = "" ;

          $data['button_save']     = $this->language->get('button_save') ;
          $data['button_cancel']   = $this->language->get('button_cancel') ;
          $data['button_send']     = $this->language->get('button_send') ;

          $data['action'] = $this->url->link('develop/option/add', 'token=' . $this->session->data['token'] . $url, true);


          $data['column_del_func']      = $this->language->get( 'column_del_func') ;
          $data['column_new_func_desc'] = $this->language->get( 'column_new_func_desc') ;
          $data['column_new_func_path'] = $this->language->get( 'column_new_func_path') ;
          $data['column_new_func']      = $this->language->get( 'column_new_func') ;

          $data['entry_del_func']       = $this->language->get( 'entry_del_func') ;
          $data['entry_new_func_desc']  = $this->language->get( 'entry_new_func_desc') ;
          $data['entry_new_func_path']  = $this->language->get( 'entry_new_func_path') ;
          $data['entry_new_func']       = $this->language->get( 'entry_new_func') ;



          if (isset($this->error)) {
               $data['error_warning'] = implode($this->error);
          } else {
               $data['error_warning'] = '';
          }

          $data['header'] = $this->load->controller('common/header');
          $data['column_left'] = $this->load->controller('common/column_left');
          $data['footer'] = $this->load->controller('common/footer');

          $this->response->setOutput($this->load->view('develop/option_form', $data));
	}

	/**
     * 驗證資料 頁面
     * @param  [type] $method [description]
     * @return [type]         [description]
     */
	protected function validateForm() {
		$path = $this->request->post['new_func_path'] ;
		$newClass = $this->request->post['new_func_class'].'.php' ;
		// 目前只檢查 controller目錄中檔案是否存在
		// todo 要做到各目錄都沒問題才行
		// dump( $this->request->post) ;
		// dump( DIR_APPLICATION.'controller/'.$path.'/'.$newClass) ;
		if ( file_exists( DIR_APPLICATION.'controller/'.$path.'/'.$newClass) && empty($this->request->post['del_func'])) {
			$this->error['exists'] = $this->language->get('error_exists');
		}
		// dump( $this->error) ;
		return !$this->error;
	}

	/**
	* 前置作業
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function preparation() {
		// 標題
		$data['heading_title'] = $this->language->get('heading_title');
		// 次標題
		$data['text_list']     = $this->language->get('text_list');
		$data['text_form']     = $this->language->get('text_add');

		// 麵包屑
		$url                   = '';
		$data['breadcrumbs']   = array();

		$data['breadcrumbs'][] = array(
		'text'                 => $this->language->get('text_home'),
		'href'                 => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
		'text'                 => $this->language->get('heading_title'),
		'href'                 => $this->url->link('develop/option', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		// 提示訊息
		$data['error_warning'] = '';
		$data['success']       = '';

		// 取消連結
		$data['cancel']        = $this->url->link('develop/option', 'token=' . $this->session->data['token'] . $url, true);

		return $data ;
     }

	/**
	* 設定功能
	* @param  [type] $method      [使用方法] controller, language, model; view另外處理
	* @param  [type] $strPath     [建立路徑]
	* @param  [type] $strFileName [使用檔名]
	* @return [type]              [description]
	*/
	protected function setupFunction( $method, $strPath, $strFileName = '') {
		// 建立目錄
		if ( !file_exists( $strPath)) {
			mkdir( $strPath, 0755) ;
		}

		switch ( $method) {
			case 'view':
				$dest = "{$strPath}/{$strFileName}_form.tpl" ;
				if ( !file_exists( $dest)) {
					copy( "{$this->pathTemplate}form.tpl", $dest) ;
				} else {

				}
				$dest = "{$strPath}/{$strFileName}_list.tpl" ;
				if ( !file_exists( $dest)) {
					copy( "{$this->pathTemplate}list.tpl", $dest) ;
				} else {

				}
			break;

			default:
				// 處理檔案
				$dest = "{$strPath}/{$strFileName}.php" ;
				$tmpExplode = explode("/", $strPath) ;
				$folderName = end( $tmpExplode) ;
				$fNameArr   = explode("_", $strFileName) ;
				$fClassArr  = array() ;
				foreach ($fNameArr as $key => $value) {
					$fClassArr[] = ucfirst($value) ;
				}

				if ( !file_exists( $dest)) {
					// dump( "檔案不存在") ;
					// 取回 template檔
					$fp     = fopen( "{$this->pathTemplate}{$method}.tmp", "r") ;
					$tmpStr = fread( $fp, filesize( "{$this->pathTemplate}{$method}.tmp")) ;
					fclose( $fp) ;

					$ctrlClassName    = ucfirst( $folderName).join( $fClassArr) ;
					$ctrlLangPath     = "{$folderName}/{$strFileName}" ;
					$modelClassName   = "{$folderName}_{$strFileName}" ;
					$langHeadingTitle = $this->request->post['new_func_desc'] ;

					$patterns = array(
						"/<CtrlClassName>/",
						"/<CtrlLangPath>/",
						"/<model_class_name>/",
						"/<lang_heading_title>/",
						"/<CreateTime>/",
					) ;
					$replacements = array(
						$ctrlClassName,
						$ctrlLangPath,
						$modelClassName,
						$langHeadingTitle,
						date('Y-m-d'),
					) ;
					// dump( $replacements) ;
					$writeStr = preg_replace( $patterns, $replacements, $tmpStr) ;
					$fp = fopen( $dest, "w") ;
					fwrite($fp, $writeStr) ;
					fclose( $fp) ;
				} else {
					$this->error['exists'] = "檔案已存在 {$dest}" ;
					return ;
				}
			break;
		}
	}


     /**
     * 移除功能
     * @param  [type] $method      [使用方法] controller, language, model; view另外處理
     * @param  [type] $strPath     [建立路徑]
     * @param  [type] $strFileName [使用檔名]
     * @return [type]              [description]
     */
     protected function removeFunction( $method, $strPath, $strFileName = '') {
          switch ( $method) {
               case 'view':
                    $dest = "{$strPath}/{$strFileName}_list.tpl" ;
                    if ( file_exists( $dest)) {
                         unlink( $dest) ;
                    }

                    $dest = "{$strPath}/{$strFileName}_form.tpl" ;
                    if ( file_exists( $dest)) {
                         unlink( $dest) ;
                    }

                    break ;
               default:
                    $dest = "{$strPath}/{$strFileName}.php" ;
                    // dump( $dest) ;
                    if ( file_exists( $dest)) {
                         unlink( $dest) ;
                    }
                    break ;
          }

          $fileCnt = (count(glob("{$strPath}/*")) === 0) ? 'Empty' : 'Not empty' ;
          // dump( $fileCnt) ;
          if ( $fileCnt == "Empty" && file_exists( $strPath)){
               rmdir( $strPath) ;
          }
     }
}