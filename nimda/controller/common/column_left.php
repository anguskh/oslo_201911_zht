<?php
class ControllerCommonColumnLeft extends Controller {
	private $menuParent = array() ;
	private $menuInfo   = array() ;

	public function index() {
        if(isset($this->request->get['act']) && $this->request->get['act']=='ajax'){
            echo $this->change();
            exit;
        }
		if (isset($this->request->get['token']) && isset($this->session->data['token']) && ($this->request->get['token'] == $this->session->data['token'])) {
			$this->load->language('common/column_left');

			$this->load->model('user/user');

			$this->load->model('tool/image');
			$this->load->model('tool/menu');

			$user_info = $this->model_user_user->getUser($this->user->getId());

			if ($user_info) {
				$data['firstname']  = $user_info['firstname'];
				$data['lastname']   = $user_info['lastname'];
				$data['username']   = $user_info['username'];
				$data['user_group'] = $user_info['user_group'];

				if (is_file(DIR_IMAGE . $user_info['image'])) {
					$data['image'] = $this->model_tool_image->resize($user_info['image'], 45, 45);
				} else {
					$data['image'] = '';
				}
			} else {
				$data['firstname']  = '';
				$data['lastname']   = '';
				$data['user_group'] = '';
				$data['image']      = '';
			}

			// 建置menu層 --------------------------------------------------------------------------------------------
			list( $this->menuParent, $this->menuInfo) = $this->model_tool_menu->getMenuStructure() ;
			// dump( $this->menuParent) ;
			// dump( $this->menuInfo) ;
			foreach ( $this->menuParent[0] as $i => $mParent) {
				if ( !isset($this->menuParent[$mParent])) {
					// Menu 資訊總覽
					// ========================================================================================================
					if ($this->user->hasPermission('access', $this->menuInfo[$mParent]['fun_path']) ||
							$this->menuInfo[$mParent]['fun_path'] == 'common/dashboard') {
						$data['menus'][] = array(
							'id'       => $this->menuInfo[$mParent]['idx'],
							'icon'	   => $this->menuInfo[$mParent]['fun_icon'],
							'name'	   => $this->language->get( $this->menuInfo[$mParent]['lang_desc']),
							'href'     => $this->url->link( $this->menuInfo[$mParent]['fun_path'], 'token=' . $this->session->data['token'], true),
							'children' => array()
						);
					}
				} else {
					$childrenArr = $this->getChildrenItems( $mParent) ;
					if ( is_array( $childrenArr)) {
						$data['menus'][] = array(
							'id'       => $this->menuInfo[$mParent]['idx'],
							'icon'	   => $this->menuInfo[$mParent]['fun_icon'],
							'name'	   => $this->language->get( $this->menuInfo[$mParent]['lang_desc']),
							'href'     => '',
							'children' => $childrenArr
						);
					}
				}
			}

			// for Angus 開發用的
			$data['menus'][count( $data['menus'])-1]['children'][] = array(
					'name'	   => "功能產生器",
					'href'     => $this->url->link('develop/option', 'token=' . $this->session->data['token'], true),
					'children' => array()
				);

            //左邊選單預設打開
            $data["col_left"] = 'active';
            if(isset($this->session->data['colLeft'])){
                $data["col_left"] = $this->session->data['colLeft'];
            }

            return $this->load->view('common/column_left', $data);
		}
	}

	private function getChildrenItems ( $mParentIdx) {
		$retArr = array() ;
		foreach ( $this->menuParent[$mParentIdx] as $i => $menuIdx) {
			if ( !isset($this->menuParent[$menuIdx])) {
				if ($this->user->hasPermission('access', $this->menuInfo[$menuIdx]['fun_path'])) {
					$retArr[] = array(
						'name'	   => $this->language->get( $this->menuInfo[$menuIdx]['lang_desc']),
						'href'     => $this->url->link( $this->menuInfo[$menuIdx]['fun_path'], 'token=' . $this->session->data['token'], true),
						'children' => array()
					);
				}
			} else {
				$childrenArr = $this->getChildrenItems( $menuIdx) ;
				if ( is_array( $childrenArr)) {
					$retArr[] = array(
						'name'	   => $this->language->get( $this->menuInfo[$menuIdx]['lang_desc']),
						'href'     => '',
						'children' => $childrenArr
					);
				}
			}
		}
		return $retArr ;
	}

    public function change(){
        if(!isset($this->session->data['colLeft']) || $this->session->data['colLeft']=='active'){
            $this->session->data['colLeft'] = '';
        }
        else{
            $this->session->data['colLeft'] = 'active';
        }

        return $this->session->data['colLeft'];
    }
}
