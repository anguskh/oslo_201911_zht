<?php
class ControllerCommonDashboard extends Controller {
	/**
	 * [資訊總覽 進入點]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2020-02-05
	 */
	public function index() {
		$this->load->language('common/dashboard');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['heading_title'] = $this->language->get('heading_title');

		// 麵包屑 Start ------------------------------------------------------------------------------
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		// todo 將來會做比較精實的部份再來改
		// Dashboard Extensions
		// Dashboard show area
		$dashboards = array() ;
		$this->load->model('extension/extension') ;

		// Get a list of installed modules
		$extensions = $this->model_extension_extension->getInstalled('dashboard');
		// dump( $extensions) ;
		// dump( $this->config) ;

		// Add all the modules which have multiple settings for each module
		foreach ($extensions as $code) {
			$output = $this->load->controller('dashboard/' . $code . '/dashboard') ;
			// dump( $output) ;

			if ($output) {
				$dashboards[] = array(
					'code'       => $code,
					'width'      => $this->config->get('dashboard_' . $code . '_width'),
					'sort_order' => $this->config->get('dashboard_' . $code . '_sort_order'),
					'output'     => $output
				);
			}
		}

		$data['rows'][] = $dashboards ;
		// dump( $data['rows']) ;

		// 第二層的資訊總覽 --> 目前寫死
		$output = $this->load->controller('dashboard/allstatus/dashboard') ;
		$data['rows'][] = array(
				array(
					'code'       => "",
					'width'      => "",
					'sort_order' => "",
					'output'     => $output
				),
		) ;

		$data['header']         = $this->load->controller('common/header');
		$data['column_left']    = $this->load->controller('common/column_left');
		$data['footer']         = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('common/dashboard', $data));
	}
}